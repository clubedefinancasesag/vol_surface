---
title: Sobre o CF
subtitle: Chegue lá. Comece aqui.
comments: false
---


## O que é o Clube de Finanças ?

O Clube de Finanças é uma liga universitária focada ao estudo de finanças. Aqui universitários estabelecem um contato com o mercado financeiro e com profissionais do ramo. Além de estudar finanças, buscamos trazer eventos, cursos, palestras e _challenges_ acerca do mercado financeiro à nossa universidade.

Os membros são estudantes de graduação do Centro de Ciências da Administração e Socioeconômicas - ESAG/UDESC em sua maioria mas também contamos com alunos da UFSC.

Nossa visão é ser uma liga de mercado financeiro referência nacional em formação de talentos.
