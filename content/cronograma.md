---
title: Cronograma de Estudos
subtitle: Núcleo de Risco e Derivativos
comments: false
---

## Processos Estocásticos (20/09/2018)

- Estudar:
    + Retornos (e taxas) continuamente compostos
        - [Wiki](https://en.wikipedia.org/wiki/Compound_interest#Continuous_compounding)
    + Movimento Browniano Geométrico
        - [Wiki MBG](https://pt.wikipedia.org/wiki/Movimento_browniano_geom%C3%A9trico)
        - [Wiki Wiener](https://pt.wikipedia.org/wiki/Processo_de_Wiener)
        - [Cap. 14 HULL][livros][^1]
        
- Vídeo-Aula:
    + [MIT Stochastic Process I](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-5-stochastic-processes-i/)
    + [MIT Stochastic Process II](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-17-stochastic-processes-ii/)
    
- Você deve saber responder as seguintes questões:
    + Qual é o limite fundamental do cálculo e como ele se relaciona aos retornos logarítimicos?
    + Pode um retorno logarítimico ser menor que -1?
    + Qual é o equivalente de um processo de Wiener em tempo discreto?
    + Por quê um processo estocástico em tempo contínuo não pode ser diferenciado com as ferramentas tradicionais do cálculo?

- Artigo para o site: Rafael

- Artigo Publicado: [Processos Estocásticos](http://clubedefinancas.com.br/materias/processos-estocasticos-para-financas-uma-introducao/)
 
## Modelo Black \& Scholes (04/10/2018)

- Estudar:
    + Modelo Black \& Scholes
        - [Wiki B\&S](https://pt.wikipedia.org/wiki/Black-Scholes)
        - [Wiki Inglês](https://en.wikipedia.org/wiki/Black%E2%80%93Scholes_model)
        - [Cap. 13 HULL][livros] _Risk Neutral Valuation_ e o Apêndice
        - [Cap. 15 HULL][livros]
    + Gregas
        - [Wiki Greeks](https://en.wikipedia.org/wiki/Greeks_(finance))
        - [Cap. 19 HULL][livros]
        
- Vídeo-Aula:
    + [MIT Ito's Lemma](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-18-ito-calculus)
    + [MIT Black \& Schole Formula and Risk Neutral Valuation](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-19-black-scholes-formula-risk-neutral-valuation)
    + [MIT Stochastic Differential Equations](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-21-stochastic-differential-equations/)
    
- Você deve saber responder as seguintes questões:
    + O que é a avaliação neutra ao risco (mundo neutro ao risco) e como ela se aplica ao modelo B\&S?
    + Quais são as premissas no modelo B\&S?
    + Quais as limitações e incoerências com os fatos empíricos que estas premissas trazem?
    + Por quê o preço de um ativo, no modelo B\&S, é modelado por um processo de MBG e não simplesmente um MB?
    + Se o preço de uma ação é descrito por um MBG, então qual é o retorno esperado de longo prazo desta ação?
    + Qual é a volatilidade anual de uma ação que possui volatilidade mensal de 4% (em termos contínuos)?
    + Quais são as gregas e gregas de segunda ordem no modelo B\&S? Qual a interpretação de cada uma?
    + Como se calcula a volatilidade implícita de uma opção?
    
- Atividade: 
    + Fazer uma Simulação de Monte Carlo com o comportamento de preço de uma ação e uma opção de compra inicialmente ATM.
    + Calibrar o modelo escolhendo uma ação real com sua volatilidade histórica
    + Simular para 10 dias a frente, com data de exercício 30 dias a frente
    + Pode-se fazer algumas poucas simulações, para verificar os diferentes caminhos entre as opções.

- Artigo para o site: Erik, Glauber
- Artigo Publicado: [O Modelo Black&Scholes](http://clubedefinancas.com.br/wp-content/uploads/2018/10/BS.html)
        
## Smile de Volatilidade (25/10/2018)

- Estudar:
    + Volatilidade implícita
        - [Cap. 20 HULL][livros]
        - [Cap. 8 DERMAN2016][livros][^6]
        - [IV Wiki](https://en.wikipedia.org/wiki/Implied_volatility)
        - [Smile Wiki](https://en.wikipedia.org/wiki/Volatility_smile)
    + Estrutura a termo da volatilidade
    + Contango e _backwardation_
        - [SA Trading Volatility I](https://seekingalpha.com/article/585481-so-you-want-to-trade-volatility)
        - [SA Trading Volatility II](https://seekingalpha.com/article/757001-so-you-want-to-trade-volatility-understanding-contango)
        - [SA Trading Volatility III](https://seekingalpha.com/article/769481-so-you-want-to-trade-volatility-understanding-backwardation)
        
- Vídeo-Aula:
    + [Volatility Surface Coursera](https://www.coursera.org/lecture/financial-engineering-2/the-volatility-surface-qgMQf)
    + [Volatility Surface in Action Coursera](https://www.coursera.org/lecture/financial-engineering-2/the-volatility-surface-in-action-FLZS8)
    
- Você deve saber responder as seguintes questões:
    + Quais as hipóteses do modelo BS que não são satisfeitas e dão origem ao smile?
    + Quais as formas possíveis de representar o smile? (quais as variáveis que podem ir no eixo x do gráfico?)
    + Por que é dito que o smile de volatilidade é uma forma de verificar a precificação de opções pelo mercado?
    + Como é possível, através do smile, inferir a distribuição de probabilidade neutra ao risco do ativo subjacente? 

- Artigo para o site: Gabriel

- Artigo publicado: [Smile de volatilidade](http://clubedefinancas.com.br/materias/smile-de-volatilidade/)

- Artigo publicado: [Smile de volatilidade - parte 2](http://clubedefinancas.com.br/materias/smile-de-volatilidade-parte-2/)

## Precificação de Opções sobre Futuros (31/10/2018)

- Estudar: 
    + Opções sobre futuros
        - [Cap. 18 HULL][livros]
        
- Vídeo-Aula:
    + [MIT Option Pricing](https://ocw.mit.edu/courses/mathematics/18-s096-topics-in-mathematics-with-applications-in-finance-fall-2013/video-lectures/lecture-20-option-price-and-probability-duality/)
    
## Volatilidade Local (14/02/2019)

- Estudar:
    + Volatilidade local
        - [Derman1994][artigos][^2]
        - [Cap. 21 e 27 HULL][livros]
        - [Cap. 10 DERMAN2016][livros]
    + Superfície local de volatilidade
        - [Local volatility](https://en.wikipedia.org/wiki/Local_volatility)
    + Formas de representação e construção
        - [Derman1995][artigos]
        - [Derman1996][artigos]
        - [Derman Lectures on the smile](http://www.emanuelderman.com/writing/entry/lectures-on-the-smile-2008)[^5]
        - [Caps. 14, 15, 16 DERMAN2016][livros]
        
- Artigo para o site: Rafael

- Artigo publicado: [Não realizado]()
        
## Superfície de volatilidade (28/02/2018)

- Estudar:
    + Métodos de interpolação
        - [Wiki Bicubic](https://en.wikipedia.org/wiki/Bicubic_interpolation)
        - [Wiki Spline](https://en.wikipedia.org/wiki/Spline_(mathematics))
        - [Wiki Spline Smoothing](https://en.wikipedia.org/wiki/Smoothing_spline)
        - [Cubic Spline](https://en.wikipedia.org/wiki/Cubic_Hermite_spline)
        - [Orosi2012][artigos]
    
- Artigo para o site: Gabriel, Rafael

- Artigo publicado: [Superfícies de volatilidade](http://clubedefinancas.com.br/materias/superficies-de-volatilidade/)
    
## Implementação em R - parte I (14/03/2018)

- Estudar: 
    + Importar os dados e manipular
        - [R for Data Science](https://r4ds.had.co.nz/)
        - [Essentials of Data Science](https://resources.rstudio.com/the-essentials-of-data-science)
        - [Dados Brutos](https://drive.google.com/drive/folders/1Sh7DEXhKLHWtMsdMt8X_al0veDBwa0X9)
    + Detectar e corrigir erros nos dados
    + O pacote RQuantLib
        - [RQuantlib](http://dirk.eddelbuettel.com/code/rquantlib.html)
        - [Quantlib](https://www.quantlib.org/)
    
- Artigo para o site: Erik, Glauber

- Artigo publicado: [Não realizado]()

## Implementação em R - parte II (28/03/2018)

- Estudar:     
    + Encontrar os preços de exercício
    + Interpolar as VI entre os preços
    + Interpolar as VI entre as maturidades
    + [Gatheral2006][livros][^7]
    
- Artigo para o site: Todos

- Artigo publicado: [Métodos de calibração de superfícies de volatilidade](http://clubedefinancas.com.br/materias/metodos-de-calibracao-de-superficies-de-volatilidade/)

- Artigo publicado: [Calibrando um SVI](http://clubedefinancas.com.br/materias/calibrando-um-svi/)

- Artigo publicado: [Superfície SVI](http://clubedefinancas.com.br/materias/superficie-svi/)

- Apresentação do projeto completo. Para audiências diversas como estudantes de finanças, até praticantes do mercado tanto buy-side quanto sell-side.
    
[^1]: [HULL][livros] Options, Futures and Other Derivatives. 9th Edition.

[^2]: [Derman1994][artigos] The Volatility Smile and Its Implied Tree.

[^3]: [Derman1995][artigos] The Local Volatility Surface.

[^4]: [Derman1996][artigos] Trading and Hedging Local Volatility.

[^5]: [DLS](http://www.emanuelderman.com/writing/entry/lectures-on-the-smile-2008) Derman Lectures on the Smile

[^6]: [DERMAN2016][livros] The Volatility Smile

[^7]: [Gatheral2006][livros] The Volatility Surface: A practioner's guide

[livros]: https://drive.google.com/drive/folders/0B2xLJTfoxbzRRVNrMFNMbDhNRW8

[artigos]: https://drive.google.com/drive/folders/1g4JPhcMIPJYlbyDEnLs5KVrttuRvJAHw