---
title: Superfície SVI
author: Rafael Bressan
date: '2019-02-22'
slug: ssvi
aliases: /post/ssvi
categories:
  - Derivativos
tags:
  - opções
  - Quant
  - SSVI
  - Volatilidade
  - superfície
bibliography: library.bib
link-citations: true
---



<p>Superfície SVI, ou somente SSVI é uma generalização do modelo <a href="/post/calibrando-uma-svi">SVI</a> de <span class="citation">(Gatheral <a href="#ref-Gatheral2004">2004</a>)</span> que busca solucionar o problema de restrição dos parâmetros do modelo para evitar a presença de arbitragem do tipo borboleta em um dado <em>smile</em>. Este modelo foi proposto por <span class="citation">(Gatheral and Jacquier <a href="#ref-Gatheral2014">2014</a>)</span> e extende o modelo SVI original apresentando duas outras parametrizações equivalentes e então o modelo para superfícies propriamente dito.</p>
<div id="reparametrizacoes-equivalentes" class="section level2">
<h2>Reparametrizações equivalentes</h2>
<p>Existem duas outras formas de se apresentar um modelo SVI que são equivalentes a parametrização <a href="/post/superficies"><em>RAW</em></a> já apresentada. Estas são as parametrizações “Natural” e “Jump-Wings” que são apresentadas abaixo.</p>
<p>Para um dado conjunto de parâmetros <span class="math inline">\(\chi_N=\{\Delta, \mu, \rho, \omega, \zeta\}\)</span> a parametrização natural de um SVI é dada por:</p>
<span class="math display" id="eq:svi-natural">\[\begin{equation}
w(k; \chi_N)=\Delta+\frac{\omega}{2}\left\lbrace 1+\zeta\rho(k-\mu)+\sqrt{(\zeta(k-\mu)+\rho)^2+(1-\rho^2)} \right\rbrace
\tag{1}
\end{equation}\]</span>
<p>onde <span class="math inline">\(\omega\geq 0\)</span>, <span class="math inline">\(\Delta, \mu \in \mathbb R\)</span>, <span class="math inline">\(|\rho|&lt;1\)</span> e <span class="math inline">\(\zeta&gt;0\)</span>. A correspondência entre as parametrizações <em>raw</em> e natural é dada pelo seguinte mapeamento e seu inverso:</p>
<span class="math display" id="eq:natural-to-raw">\[\begin{equation}
(a, b, \rho, m, \sigma)=\left(\Delta+\frac{\omega}{2}(1-\rho^2), \frac{\omega\zeta}{2}, \rho, \mu-\frac{\rho}{\zeta}, \frac{\sqrt{1-\rho^2}}{\zeta}\right)
\tag{2}
\end{equation}\]</span>
<span class="math display" id="eq:raw-to-natural">\[\begin{equation}
(\Delta, \mu, \rho, \omega, \zeta)=\left(a-\frac{\omega}{2}(1-\rho^2), m+\frac{\rho\sigma}{\sqrt{1-\rho^2}}, \rho, \frac{2b\sigma}{\sqrt{1-\rho^2}}, \frac{\sqrt{1-\rho^2}}{\sigma}\right)
\tag{3}
\end{equation}\]</span>
<p>A desvantagem destas parametrizações é que o valor de seus parâmetros não são intuitivos para os <em>traders</em>, eles não carregam estes valores em sua memória durante a negociação. Valores característicos de uma superfície de volatilidade implícita que <em>traders</em> têm em mente são, por exemplo, volatilidade ATM, <em>skew</em> de volatilidade ATM e assíntotas. Desta forma a parametrização <em>Jump-Wings</em> é útil, pois relaciona estes valores típicos aos parâmetros <em>raw</em> de um SVI.</p>
<p>A parametrização <em>JW</em> é dada em termos da variância implícita (e não da variância total) e portanto existe uma dependência explícita do tempo em sua formulação. Para um dado tempo até a expiração, <span class="math inline">\(\tau\)</span>, o conjunto de parâmetros <span class="math inline">\(\chi_{J}=\{v_\tau, \psi_\tau, p_\tau, c_\tau, \tilde v_\tau\}\)</span> é definido pelas seguintes equações a partir dos parâmetros <em>raw</em>:</p>
<span class="math display" id="eq:raw-to-jw">\[\begin{align}
v_\tau&amp;=\frac{a+b\{-\rho m + \sqrt{m^2+\sigma^2}\}}{\tau},\\
\psi_\tau&amp;=\frac{1}{\sqrt{w_\tau}}\frac{b}{2}\left(-\frac{m}{\sqrt{m^2+\sigma^2}}+\rho\right),\\
p_\tau&amp;=\frac{1}{\sqrt{w_\tau}}b(1-\rho),\\
c_\tau&amp;=\frac{1}{\sqrt{w_\tau}}b(1+\rho),\\
\tilde v_\tau&amp;=\frac{1}{\tau}\left(a+b\sigma\sqrt{1-\rho^2}\right)
\tag{4}
\end{align}\]</span>
<p>onde <span class="math inline">\(w_\tau=v_\tau \tau\)</span> relaciona a variância total ATM com a variância ATM. Os parâmetros possuem as seguintes interpretações: <span class="math inline">\(v_\tau\)</span> é a variância ATM, <span class="math inline">\(\psi_\tau\)</span> o <em>skew</em> ATM, <span class="math inline">\(p_\tau\)</span> a inclinação da asa esquerda (puts), <span class="math inline">\(c_\tau\)</span> a inclinação da asa direita (calls) e <span class="math inline">\(\tilde v_\tau\)</span> é a variância implícita mínima.</p>
<p>A figura <a href="#fig:svi-jw">1</a> apresenta uma esquematização destes parâmetros sobre um <em>smile</em> fictício para melhor compreensão.</p>
<div class="figure"><span id="fig:svi-jw"></span>
<img src="/images/svi-jw.png" alt="Interpretação dos parâmetros de um SVI-JW."  />
<p class="caption">
Figura 1: Interpretação dos parâmetros de um SVI-JW.
</p>
</div>
<p>As relações inversas que trazem uma parametrização <em>JW</em> para uma <em>raw</em>, assumindo que <span class="math inline">\(m \neq 0\)</span> são:</p>
<span class="math display" id="eq:jw-to-raw">\[\begin{align}
b&amp;=\frac{\sqrt{w_\tau}}{2}(c_\tau+p_\tau),\\
\rho&amp;=1-\frac{p_\tau\sqrt{w_\tau}}{b},\\
a&amp;=\tilde v_\tau \tau-b\sigma\sqrt{1-\rho^2},\\
m&amp;=\frac{(v_\tau-\tilde v_\tau)\tau}{b\left\lbrace-\rho+sign(\alpha)\sqrt{1+\alpha^2}-\alpha\sqrt{1-\rho^2}\right\rbrace},\\
\sigma&amp;=\alpha m.
\tag{5}
\end{align}\]</span>
<p>onde as variáveis auxiliares são definidas da seguinte forma: <span class="math inline">\(\beta:=\rho-(2\psi_\tau\sqrt{w_\tau})/b\)</span> e <span class="math inline">\(\alpha:=sign(\beta)\sqrt{1/\beta^2 - 1}\)</span>, com <span class="math inline">\(\beta \in [-1, 1]\)</span> para garantir a convexidade do <em>smile</em>.</p>
<p>Se <span class="math inline">\(m=0\)</span>, então as equações para <span class="math inline">\(a, b, \rho\)</span> se mantêm, porém, <span class="math inline">\(\sigma = (v_\tau \tau-a)/b\)</span>. Desta forma temos relações entre as três parametrizações SVI, sendo possível navegar entre elas com tranquilidade. Um <em>trader</em> pode verificar no mercado os valores dos parâmetros <em>JW</em> e traduzi-los para <em>raw</em> e simular o <em>smile</em> ou fazer o caminho reverso, calibrar uma fatia da superfície com parâmetros <em>raw</em>, traduzi-los para <em>JW</em> e apresentar para sua mesa, onde todos conseguirão interpretar os valores a que estão habituados.</p>
</div>
<div id="superficie-svi" class="section level2">
<h2>Superfície SVI</h2>
<p>Uma SSVI surge como uma extensão à parametrização natural de um SVI, e fornece em uma única equação, a possibilidade de parametrizar uma superfície de volatilidade implícita por inteiro. É necessário, antes de mais nada, fazer algumas definições preliminares. Defina a variância total implícita no dinheiro (ATM) como <span class="math inline">\(\theta_\tau:=\sigma_{BS}^2(0, \tau)\tau\)</span> e <span class="math inline">\(\lim\limits_{\tau\rightarrow 0}\theta_\tau = 0\)</span>.</p>

<div class="definition">
<span id="def:ssvi" class="definition"><strong>Definição 1  </strong></span>Seja <span class="math inline">\(\varphi\)</span> uma função suave em <span class="math inline">\(\mathbb R_+^*\mapsto \mathbb R_+^*\)</span> tal que <span class="math inline">\(\lim\limits_{\tau\rightarrow 0}\theta_\tau \varphi(\theta_\tau)\)</span> exista em <span class="math inline">\(\mathbb R\)</span>. Uma SSVI é definida por:
</div>

<span class="math display" id="eq:ssvi">\[\begin{equation}
w(k, \theta_\tau)=\frac{\theta_\tau}{2}\left\lbrace 1+\rho\varphi(\theta_\tau)k+\sqrt{(\varphi(\theta_\tau)k+\rho)^2+(1-\rho^2)} \right\rbrace
\tag{6}
\end{equation}\]</span>
<p>Veja que na representação SSVI, engenhosamente os autores substituíram a dimensão de tempo, <span class="math inline">\(\tau\)</span>, típica de superfícies de volatilidde, pela variância total ATM. Com esta representação, eles conseguiram derivar as condições necessárias para a ausência de arbitragem estática na superfície e sempre que necessário, é possível retornar a dimensão de tempo no calendário.</p>
<p>Agora é necessário definir a função <span class="math inline">\(\varphi\)</span>, que então será substituída na equação <a href="#eq:ssvi">(6)</a> da definição <a href="#def:ssvi">1</a> com seus próprios parâmetros e teremos por fim uma função <span class="math inline">\(w(k, \theta_\tau; \chi_{ssvi})\)</span> que poderá ser calibrada para o conjunto de parâmetros da SSVI, <span class="math inline">\(\chi_{ssvi}\)</span> contra os dados observados no mercado. No fundo, qualquer função que obedeça as condições impostas na definição <a href="#def:ssvi">1</a> pode ser utilizada, entretanto os autores apresentam dois tipos de função que condizem com as observações empíricas.</p>
<div id="heston" class="section level3">
<h3>Heston</h3>
<p>Considere a função <span class="math inline">\(\varphi\)</span> definida por:</p>
<span class="math display" id="eq:heston-phi">\[\begin{equation}
\varphi(\theta)\equiv\frac{1}{\gamma\theta}\left\lbrace 1-\frac{1-e^{-\gamma\theta}}{\gamma\theta}\right\rbrace
\tag{7}
\end{equation}\]</span>
<p>com <span class="math inline">\(\gamma &gt; 0\)</span>. Esta função recebeu este nome pois, seu <em>skew</em> na variância implícita é compatível com aquele previsto no modelo de <a href="/post/superficies">Heston</a>.</p>
</div>
<div id="lei-de-potencia" class="section level3">
<h3>Lei de potência</h3>
<p>A parametrização da função <span class="math inline">\(\varphi\)</span> como uma lei de potência é primeiramente considerada da seguinte forma: <span class="math inline">\(\varphi(\theta)=\eta\theta^{-\gamma}\)</span> com <span class="math inline">\(\eta &gt; 0\)</span> e <span class="math inline">\(0&lt;\gamma&lt;1\)</span>. Entretanto, esta parametrização apresenta algumas limitações com relação a arbitragem do tipo borboleta e então é proposta a seguinte forma funcional:</p>
<span class="math display" id="eq:pl-phi">\[\begin{equation}
\varphi(\theta)=\frac{\eta}{\theta^\gamma(1+\theta)^{1-\gamma}}
\tag{8}
\end{equation}\]</span>
<p>que é <strong>garantida</strong> não possuir arbitragem estática dado que <span class="math inline">\(\eta(1+|\rho|)\leq 2\)</span>.</p>
</div>
</div>
<div id="condicoes-de-nao-arbitragem" class="section level2">
<h2>Condições de não-arbitragem</h2>
<p>As condições para ausência de arbitragem estática para uma SSVI são colocadas através de dois teoremas (4.1 e 4.2) e provadas no artigo de <span class="citation">(Gatheral and Jacquier <a href="#ref-Gatheral2014">2014</a>)</span>, dos quais resulta o seguinte corolário.</p>

<div class="corollary">
<p><span id="cor:ssvi-arbitrage" class="corollary"><strong>Corolário 1  </strong></span>A superfície SVI definida em <a href="#def:ssvi">1</a> está livre de arbitragem estática se as seguintes condições são satisfeitas:</p>
<ol style="list-style-type: decimal">
<li><span class="math inline">\(\partial_\tau\theta_\tau\geq 0, \text{ para todo } \tau &gt; 0\)</span></li>
<li><span class="math inline">\(0\leq \partial_\theta(\theta\varphi(\theta))\leq\frac{1}{\rho^2}\left(1+\sqrt{1-\rho^2}\right)\varphi(\theta), \text{ para todo } \theta&gt;0\)</span></li>
<li><span class="math inline">\(\theta\varphi(\theta)(1+|\rho|)&lt;4, \text{ para todo } \theta&gt;0\)</span></li>
<li><span class="math inline">\(\theta\varphi(\theta)^2(1+|\rho|)\leq 4, \text{ para todo } \theta&gt;0\)</span>
</div>
</li>
</ol>
<p>Onde os dois primeiros itens dizem respeito a ausência de arbitragem de calendário, enquanto que os seguintes são exigências para a superfície estar livre de arbitragem do tipo borboleta.</p>
<p>Para uma função <span class="math inline">\(\varphi\)</span> do tipo Heston, as condições nos seus parâmetros são: <span class="math inline">\(\gamma&gt;0\)</span> que garante o atendimentos as condições 1 e 2 do corolário <a href="#cor:ssvi-arbitrage">1</a> e <span class="math inline">\(\gamma\geq(1+|\rho|)/4\)</span> para garantir a ausência de arbitragem borboleta, a qual subsume a primeira condição.</p>
<p>Para a função do tipo lei de potência dada em <a href="#eq:pl-phi">(8)</a> são necessárias as condições <span class="math inline">\(0&lt;\gamma&lt;1\)</span> e <span class="math inline">\(\eta(1+|\rho|)\leq 2\)</span></p>
<p>A maneira típica de impor as restrições dos parâmetros no momento da calibração do modelo é inserir uma penalidade na função objetivo, quando a restrição é violada. Por exemplo, consideramos a restrição de inequalidade para a lei de potência, <span class="math inline">\(\eta(1+|\rho|)-2\leq 0\)</span>. No momento da calibração, nós devemos calcular o valor desta expressão e se o seu resultado for maior que zero, uma penalidade é somada a função objetivo da calibração (em geral a soma dos quadrados dos erros).</p>
</div>
<div id="densidade-neutra-ao-risco" class="section level2">
<h2>Densidade neutra ao risco</h2>
<p>Já comentamos sobre a <a href="/post/smile-parte-2">distribuição neutra ao risco</a> implícita no preço de opções que pode ser obtida através da fórmula de <span class="citation">(Breeden and Litzenberger <a href="#ref-Breeden1978">1978</a>)</span>, assim como já foi introduzida uma certa função <a href="/post/calibrando-uma-svi"><span class="math inline">\(g(k)\)</span></a> que desempenha um papel fundamental para garantir a ausência de arbitragem do tipo borboleta em <em>smiles</em> de variância total. O fato é que, para garantir a ausência de arbitragem a função <span class="math inline">\(g(k)\)</span> deve ser não negativa em todo o seu suporte <span class="math inline">\(k \in \mathbb R\)</span>. Ao mesmo tempo, a definição de ausência de arbitragem borboleta se confunde com o fato que a densidade neutra ao risco deve também ser não negativa, caso contrário não seria uma densidade de probabilidade. Logo percebe-se a estreita relação entre a função <span class="math inline">\(g(k)\)</span> e a densidade neutra ao risco implícita no preço de opções. De fato, a fórmula de Breeden-Litzenberger nos fornece:</p>
<span class="math display">\[\begin{equation}
p(k)=\left.\frac{\partial^2C_B(k, w(k))}{\partial K^2}\right|_{K=Fe^k} 
\end{equation}\]</span>
<p>que após fazer as derivações da equação de Black e as substituições e manipulações algébricas com as reparametrizações do modelo B&amp;S, resulta em:</p>
<span class="math display" id="eq:pk">\[\begin{equation}
p(k)=\frac{g(k)}{\sqrt{2\pi w(k)}}\exp\left(-\frac{d_2(k)^2}{2}\right)
\tag{9}
\end{equation}\]</span>
<p>E para relembrarmos, a função <span class="math inline">\(g(k)\)</span> é dada por:</p>
<span class="math display" id="eq:g">\[\begin{equation}
g(k)=\left(1-\frac{kw\prime(k)}{2w(k)}\right)^2-\frac{w\prime(k)^2}{4}\left(\frac{1}{w(k)}+\frac{1}{4}\right)+\frac{w\prime\prime(k)}{2}
\tag{10}
\end{equation}\]</span>
<p>Ou seja, uma vez parametrizado um SVI para um dado prazo de maturidade, <span class="math inline">\(\tau\)</span>, se torna simples a tarefa de extrair a densidade implícita. Possuímos a função <span class="math inline">\(w(k)\)</span> e suas derivadas e certamente <span class="math inline">\(d_2(k)\)</span>, bastando portanto, aplicar a equação <a href="#eq:pk">(9)</a> para extrair importante informação do mercado de opções.</p>
</div>
<div id="superficie-de-volatilidade-local" class="section level2">
<h2>Superfície de volatilidade local</h2>
<p>De maneira semelhante ao procedimento realizado com a densidade implícita, também é possível, uma vez parametrizada a SSVI, derivar a <strong>superfície de volatilidade local</strong> através da <a href="/post/superficies">equação</a> de <span class="citation">(Dupire <a href="#ref-Dupire1994">1994</a>)</span>. Esta equação toma a seguinte forma funcional:</p>
<span class="math display" id="eq:dupire">\[\begin{equation}
\sigma_L^2(K, \tau)=\frac{\partial_\tau C_B(K, \tau)}{\frac{1}{2}K^2\partial_{KK}C_B(K, \tau)}
\tag{11}
\end{equation}\]</span>
<p>Novamente tomando as derivadas da equação de Black e fazendo as substituições necessárias<a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a> chegamos a relação entre a superfície SVI e variância local.</p>
<span class="math display" id="eq:var-local">\[\begin{equation}
\sigma^2_L(k, \tau)=\frac{\partial_\tau w(k, \theta_\tau)}{g(k, w(k, \theta_\tau))}
\tag{12}
\end{equation}\]</span>
<p>De forma bastante simplista, a superfície de volatilidade local pode ser entendida como aquela superfície de volatilidades instantâneas para o ativo subjacente, <span class="math inline">\(\sigma(S, t)\)</span> que depende tanto do nível de preço deste ativo quanto do tempo, e fornece a previsão do mercado para a volatilidade instantânea de <span class="math inline">\(S\)</span> dado que este ativo hoje está precificado em <span class="math inline">\(S_0\)</span> e no tempo futuro <span class="math inline">\(\tau\)</span> estará em <span class="math inline">\(K\)</span>. Fazendo uma analogia com o mercado de juros, a superfície local está para a curva <em>forward</em> assim como a superfície implícita está para a curva de juros, numa aproximação.</p>
<p>A superfície local é muito utilizada para precificar opções exóticas, aquelas que possuem perfil de <em>payoff</em> distinto das opções europeias e podem ou não terem seus resultados atrelados ao caminho seguido pelo preço do ativo objeto durante a vida da opção. Nestes casos, a precificação se dá através da incorporação da superfície local estipulando um valor de volatilidade instantânea para cada possível combinação <span class="math inline">\((S_t, t)\)</span> em geral em um ambiente de simulação de <a href="/post/monte-carlo">Monte Carlo</a>.</p>
<p>No caso da equação <a href="#eq:var-local">(12)</a> temos uma nova derivada a ser computada, <span class="math inline">\(\partial_\tau w(k, \theta_\tau)\)</span>, que só poderia ser feita de forma analítica caso <span class="math inline">\(\theta_\tau\)</span> fosse parametrizada explicitamente. Nem sempre este é o caso, já que <span class="math inline">\(\theta_\tau\)</span> são os valores de variância total ATM implícitas, ou seja, observa-se no mercado apenas alguns <strong>pontos</strong> de <span class="math inline">\(\theta_\tau\)</span>, pontos estes que podem estar espaçados em intervalos diferentes de tempo. A solução para esta derivação é interpolar estes pontos, nem que seja uma simples interpolação linear, e então fazer a derivação de forma numérica através de um método de diferenças finitas. Note que na interpolação deve-se garantir a condição de não arbitragem de calendário, <span class="math inline">\(\partial_\tau\theta_\tau\geq 0\)</span>.</p>
</div>
<div id="calibracao" class="section level2">
<h2>Calibração</h2>
<p>Vamos retomar nosso exemplo de superfície de volatilidade do <a href="/post/calibrando-uma-svi">artigo anterior</a>, porém agora com todas as datas de expiração, ou seja, a superfície completa. Os códigos <a href="https://cran.r-project.org/">R</a> apresentados abaixo ajudam na compreensão do procedimento.</p>
<pre class="r"><code>library(readr)
library(dplyr)
library(purrr)
library(kableExtra)
library(ggplot2)
library(ggthemes)
library(plot3D)
source(&#39;svi.R&#39;)
source(&#39;ssvi.R&#39;)</code></pre>
<p>Primeiramente foram carregados os pacotes necessários para a leitura e manipulação dos dados (<code>readr</code>, <code>dplyr</code> e <code>purrr</code>) assim como os pacotes de visualização (<code>kableExtra</code>, <code>ggplot2</code>, <code>ggthemes</code> e <code>plot3D</code>). Em seguida os arquivos <code>svi.R</code> e <code>ssvi.R</code> são implementações do <a href="http://clubedefinancas.com.br/">Clube de Finanças</a> para as funções necessárias para calibrar uma SSVI.</p>
<p>Carregados os pacotes e as funções, deve-se carregar os dados da superfície de volatilidade e organizá-los da forma que necessitamos.</p>
<pre class="r"><code>ssvi_data &lt;- read_csv(&quot;../../static/input/IV_Raw_Delta_surface.csv&quot;,
                     col_types = cols(date = col_date(format = &quot;%m/%d/%Y&quot;))) %&gt;% 
  mutate(tau = period / 365,
         theta = theta_vec(moneyness, tau, iv)) %&gt;% 
  rename(k = moneyness)</code></pre>
<p>Para calibrar os parâmetros de uma SSVI, com a função <span class="math inline">\(\varphi\)</span> do tipo <strong>lei de potência</strong>, necessitaremos dos dados de <em>moneyness</em> (<span class="math inline">\(k\)</span>), vencimento (<span class="math inline">\(\tau\)</span>) e volatilidade implícita (<span class="math inline">\(iv\)</span>) que será internamente convertida em variância total implícita (<span class="math inline">\(w\)</span>). Dentro da função de ajuste dos parâmetros <code>fit_ssvi()</code> a variância total ATM implícita <span class="math inline">\(\theta_\tau\)</span> é calculada através de interpolação <em>spline</em> para cada uma das fatias da superfície, pois, nossos dados não necessariamente possuem esta observação. Cabe ressaltar também que os dados estão organizados de forma <code>tidy</code>, sendo portanto, todos os argumentos passados para as funções na forma de vetores, não sendo necessário gerar uma matriz contendo o <em>grid</em> <span class="math inline">\((k, \tau)\)</span>.</p>
<pre class="r"><code>k &lt;- ssvi_data$k
tau &lt;- ssvi_data$tau
iv &lt;- ssvi_data$iv
theta &lt;- ssvi_data$theta

set.seed(12345)
powerlaw_par &lt;- fit_ssvi(tau, k, iv, &quot;powerlaw&quot;)
kable(powerlaw_par,
      caption = &quot;Parâmetros da SSVI-power-law estimados.&quot;,
      col.names = &quot;SSVI-PL&quot;)</code></pre>
<table>
<caption>
<span id="tab:ssvi-cal">Tabela 1: </span>Parâmetros da SSVI-power-law estimados.
</caption>
<thead>
<tr>
<th style="text-align:left;">
</th>
<th style="text-align:right;">
SSVI-PL
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
rho
</td>
<td style="text-align:right;">
-0.6479238
</td>
</tr>
<tr>
<td style="text-align:left;">
gamma
</td>
<td style="text-align:right;">
0.4926757
</td>
</tr>
<tr>
<td style="text-align:left;">
eta
</td>
<td style="text-align:right;">
0.8607807
</td>
</tr>
</tbody>
</table>
<p>Podemos rapidamente checar se estes parâmetros estimados geram uma superfície livre de arbitragem.</p>
<pre class="r"><code>paste(&quot;Parametrização livre de arbitragem borboleta? :&quot;, 
      ssvi_butterfly_cons(powerlaw_par, &quot;powerlaw&quot;) &lt;= 0)</code></pre>
<pre><code>## [1] &quot;Parametrização livre de arbitragem borboleta? : TRUE&quot;</code></pre>
<p>Com os parâmetros estimados, é simples plotar todos os <em>smiles</em> que compõe a superfície e verificar visualmente o ajuste. Vamos plotar a <strong>variância total</strong> em função do <em>moneyness</em> e vencimentos, pois neste gráfico se verifica a condição de arbitragem de calendário simplesmente através do fato que as curvas geradas não devem se cruzar.</p>
<pre class="r"><code>plt_df &lt;- ssvi_data %&gt;% 
  mutate(w_pl = ssvi_fun(powerlaw_par, theta, k)) %&gt;% 
  select(k, tau, w_pl, iv) %&gt;% 
  filter(tau &lt; 0.5)

ggplot(plt_df, aes(x = k, y = w_pl)) + 
  geom_line(aes(color = as.factor(format(tau, digits = 3)))) +
  geom_point(aes(y = iv^2 * tau)) +
  guides(color = guide_legend(title = &quot;Vencimento&quot;)) +
  labs(title = &quot;SSVI Lei de Potência&quot;,
       x = &quot;Forward log-moneyness (k)&quot;,
       y = &quot;Variância total implícita (w)&quot;,
       caption = &quot;Elaborado por Rafael Bressan para o Clube de Finanças.&quot;) +
#  scale_y_continuous(labels = scales::percent) +
  scale_color_viridis_d() +
  theme_economist_white()</code></pre>
<p><img src="/post/2019-02-22-superf%C3%ADcie-svi_files/figure-html/ssvi_plot-1.png" width="864" /></p>
<p>Foram retirados os vencimentos mais longos apenas para uma melhor visualização da parte mais curta da superfície, que em geral é a mais complicada de se calibrar através de SVI. O resultado parece interessante, com as variâncias ao redor do dinheiro tendo boa aderência aos dados observados, porém para <em>puts</em> muito fora do dinheiro, a parametrização aparenta <strong>subestimar</strong> o valor da variância total e portanto estaria subprecificando as opções.</p>
<p>Iremos agora verificar a <em>densidade da distribuição implícita</em> para o período de vencimento de 90 dias. Utiliza-se para tanto a equação <a href="#eq:pk">(9)</a> onde <span class="math inline">\(w(k)\)</span> (e suas derivadas e o próprio <span class="math inline">\(d_2\)</span>) será agora calculado com base nos parâmetros da tabela <a href="#tab:ssvi-cal">1</a> para um vetor de <em>moneyness</em> mais amplo e denso. O resultado pode ser observado através da figura <a href="#fig:ssvi-dens">2</a></p>
<pre class="r"><code>thetadens &lt;- ssvi_data %&gt;% 
  filter(period == 90) %&gt;% 
  pull(theta) %&gt;% 
  `[`(1)
kdens &lt;- seq(-0.5, 0.3, length.out = 100)
dens &lt;- ssvi_density(powerlaw_par, thetadens, kdens, &quot;powerlaw&quot;)
dens_tbl &lt;- tibble(kdens = kdens, dens = dens)

ggplot(dens_tbl, aes(x = kdens, y = dens)) + 
  geom_line() +
  labs(title = &quot;Densidade neutra ao risco SSVI&quot;,
     x = &quot;Forward log-moneyness (k)&quot;,
     y = &quot;Densidade&quot;,
     caption = &quot;Elaborado por Rafael Bressan para o Clube de Finanças.&quot;) +
  scale_color_viridis_d() +
  theme_economist_white()</code></pre>
<div class="figure"><span id="fig:ssvi-dens"></span>
<img src="/post/2019-02-22-superf%C3%ADcie-svi_files/figure-html/ssvi-dens-1.png" alt="Densidade implícita estimada. Presença de assimetria e leptocurtose a esquerda." width="864" />
<p class="caption">
Figura 2: Densidade implícita estimada. Presença de assimetria e leptocurtose a esquerda.
</p>
</div>
<p>Este é um gráfico interessante, que nos mostra exatamente o que se espera de um <em>smile</em> típico de <em>equities</em> que possui <em>skew</em> negativo. Percebemos como a densidade de probabilidades é assimétrica, com a cauda esquerda muito mais longa, refletindo o sentimento de mercado de uma maior probabilidade de grandes quedas no preço do ativo objeto que altas equivalentes. Sempre que verificamos um <em>smile</em> com <em>skew</em> negativo, como é o presente caso, a distribuição de probabilidades é assimétrica a esquerda.</p>
<p>Vamos conferir se a área sob esta curva de densidade integra aproximadamente 1, como deve ser.</p>
<pre class="r"><code>area &lt;- integrate(function(x) ssvi_density(powerlaw_par, thetadens, x, &quot;powerlaw&quot;),
                  lower = kdens[1],
                  upper = kdens[length(kdens)])
paste(&quot;Área sob a curva de densidade é: &quot;, area$value)</code></pre>
<p>[1] “Área sob a curva de densidade é: 0.999302879362191”</p>
<p>Chegou o momento de inferirmos a <strong>superfície de volatilidade local</strong> a partir de nossa parametrização SSVI. O método será a aplicação direta da equação <a href="#eq:var-local">(12)</a> pois, com a parametrização realizada, dispomos de todos os dados necessários para seu cômputo.</p>
<p>Antes porém, devemos observar uma peculiaridade dos nossos dados. O site <a href="https://ivolatility.com">ivolatility.com</a> fornece dados alinhados por <strong>Delta</strong>, ou seja, para cada período de vencimento temos um mesmo conjunto de Deltas, <strong>mas não de moneyness</strong>. Os valores deste último irá variar conforme o vencimento, uma vez que pela definição de <em>forward log-moneyness</em> que vimos utilizando é dependente do tempo para maturidade da opção. Desta forma precisamos gerar um novo <em>grid</em> <span class="math inline">\((k, \tau)\)</span> para plotar nossa superfície de volatilidade local.</p>
<p>Para tanto, criaremos uma sequência uniforme de valores de <span class="math inline">\(k\)</span> e tomaremos os valores que já possuímos de <span class="math inline">\(\tau\)</span> e <span class="math inline">\(\theta_\tau\)</span> recombinando-os através da função <code>expand.grid()</code> para gerar o data frame que precisamos.</p>
<p>Feito isso, é apenas questão de aplicar a equação <a href="#eq:var-local">(12)</a> e criar uma matriz (pois assim pede a função <code>surf3D()</code>) com os valores da volatilidade local.</p>
<pre class="r"><code>kloc &lt;- seq(-.4, 0.4, length.out = 17)
utau &lt;- unique(tau)
utheta &lt;- unique(theta)
names(utheta) &lt;- utau  # utheta will be a lookup vector
grid_df &lt;- expand.grid(kloc, utau) %&gt;% 
  rename(kloc = Var1,
         tau = Var2) %&gt;% 
  mutate(theta = utheta[as.character(tau)])

loc_vol_vec &lt;- ssvi_local_vol(powerlaw_par, 
                              grid_df$tau, 
                              grid_df$theta, 
                              grid_df$kloc, 
                              &quot;powerlaw&quot;)
# Matrix where k is row
loc_vol_m &lt;- matrix(loc_vol_vec, nrow = length(kloc))  

# Plot Local Volatility Surface
# x is k, y is tau
M &lt;- mesh(kloc, utau)
surf3D(M$x, M$y, loc_vol_m, colkey = FALSE, bty = &quot;b2&quot;, 
       phi = 20, ticktype = &quot;detailed&quot;,
       xlab = &quot;k&quot;,
       ylab = &quot;\u03c4&quot;,
       zlab = &quot;Volatilidade local \u03c3&quot;)</code></pre>
<p><img src="/post/2019-02-22-superf%C3%ADcie-svi_files/figure-html/vol-local-1.png" width="864" /></p>
</div>
<div id="conclusao" class="section level2">
<h2>Conclusão</h2>
<p>Apresentamos o modelo de superfícies SVI, que faz uma generalização dos <em>smiles</em> SVI e apresenta vantagens sobre a parametrização fatia-a-fatia em virtude dos teoremas sobre arbitragem estática, apresentando restrições para os parâmetros do modelo que garantem a ausência deste tipo de arbitragem.</p>
<p>Uma vez parametrizada toda uma SSVI, torna-se simples, uma mera aplicação de fórmulas a obtenção tanto da densidade da distribuição neutra ao risco implícita no preços das opções, como da superfície de volatilidade local através da equação de Dupire.</p>
</div>
<div id="referencias" class="section level2 unnumbered">
<h2>Referências</h2>
<div id="refs" class="references">
<div id="ref-Breeden1978">
<p>Breeden, Douglas T, and Robert H Litzenberger. 1978. “Prices of State-Contingent Claims Implicit in Option Prices.” <em>Journal of Business</em>. JSTOR, 621–51.</p>
</div>
<div id="ref-Dupire1994">
<p>Dupire, Bruno. 1994. “Pricing with a Smile.” <em>Risk</em> 7 (1): 18–20.</p>
</div>
<div id="ref-Gatheral2004">
<p>Gatheral, Jim. 2004. “A Parsimonious Arbitrage-Free Implied Volatility Parameterization with Application to the Valuation of Volatility Derivatives.” <em>Presentation at Global Derivatives &amp; Risk Management, Madrid</em>.</p>
</div>
<div id="ref-Gatheral2014">
<p>Gatheral, Jim, and Antoine Jacquier. 2014. “Arbitrage-Free Svi Volatility Surfaces.” <em>Quantitative Finance</em> 14 (1). Taylor &amp; Francis: 59–71.</p>
</div>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Referimos o leitor ao material do prof. Antoine Jacquier para uma prova desta relação <a href="http://wwwf.imperial.ac.uk/~ajacquie/IC_AMDP/IC_AMDP_Docs/AMDP.pdf">aqui</a><a href="#fnref1">↩</a></p></li>
</ol>
</div>
