---
title: Calibrando uma SVI
author: Rafael Bressan
date: '2019-02-15'
categories:
  - Derivativos
tags:
  - opções
  - Quant
  - R
  - SVI
  - Volatilidade
slug: calibrando-uma-svi
aliases: /post/calibrando-uma-svi
bibliography: library.bib
link-citations: true
---

<script src="/rmarkdown-libs/kePrint/kePrint.js"></script>
<link href="/rmarkdown-libs/bsTable/bootstrapTable.min.css" rel="stylesheet" />


<p>Neste post iremos mostrar como fazer uma calibração de um smile SVI baseado nos trabalhos de <span class="citation">(Gatheral <a href="#ref-Gatheral2004">2004</a>)</span> e <span class="citation">(De Marco and Martini <a href="#ref-DeMarco2009">2009</a>)</span>. Escolheremos apenas uma fatia da superfície de volatilidade, fixando o tempo para expiração (maturidade) e coletando as volatilidades implícitas para diversos strikes.</p>
<p>Como já apresentado em posts anteriores, existem diversas formas de interpolar, extrapolar, parametrizar e calibrar smiles de volatilidade. Exsitem vantagens e desvantagens para cada método. Neste post iremos fixar nossa atenção no modelo paramétrico de smile chamado SVI - Stochastic Volatility Inspired - uma forma que une a “simplicidade” de um modelo paramétrico com o poder de adesão aos dados de mercado dos modelos de volatilidade estocástica (i.e. Heston, SABR e afins).</p>
<p>Chamar o modelo SVI de simples é puro eufemismo, ele é um modelo poderoso, com fundamento teórico avançado e diversos detalhes para sua calibração.</p>
<div id="modelo-svi" class="section level2">
<h2>Modelo SVI</h2>
<p>Este modelo foi apresentado por <a href="https://mfe.baruch.cuny.edu/jgatheral/">Jim Gatheral</a> na conferência <em>Global Derivatives &amp; Risk Management 2004</em> e foi bem recebido pelos profissionais de mercado interessados em superfícies de volatilidade para <em>equities</em>, principalmente.</p>
<p>O modelo possui duas propriedades que são as razões para sua popularidade. Ele satisfaz a fórmula do momento de <span class="citation">Lee (<a href="#ref-Lee2004">2004</a>)</span>, que é um resultado independente de modelo que especifica os limites assintóticos para um <em>smile</em> de volatilidade implícita. Portanto, o modelo SVI é válido para extrapolação além da região central dos dados disponíveis. Além disso, afirma-se que o modelo SVI é relativamente fácil de calibrar para dados de mercado, de modo que a superfície de volatilidade implícita correspondente é livre de arbitragem de calendário. As condições que garantem a ausência de arbitragem de borboleta forma resolvidas em um segundo artigo por <span class="citation">Gatheral and Jacquier (<a href="#ref-Gatheral2014">2014</a>)</span>.</p>
<p>No SVI é possível se estabelecer condições explícitas em seus parâmetros, de modo que o modelo não gere preços onde oportunidades de <a href="/post/smile-parte-2">arbitragem estáticas</a> possam ocorrer. A calibração para dados reais de mercado requer algoritmos de otimização não-linear e pode ser bastante demorada. Mais recentemente, um método para calibração que usa a estrutura inerente do modelo para reduzir as dimensões do problema de otimização foi desenvolvido em <span class="citation">De Marco and Martini (<a href="#ref-DeMarco2009">2009</a>)</span>.</p>
<p>A parametrização conhecida como <em>RAW</em> do SVI é apresentada na equação <a href="#eq:rawsvi">(1)</a>, seguindo a notação já introduzida <a href="/post/smile">anteriormente</a>, portanto, estamos modelando a <strong>variância total</strong> implícita para um determinado prazo. Para diferentes maturidades, teremos diferentes conjuntos de parâmetros.</p>
<span class="math display" id="eq:rawsvi">\[\begin{equation}
w(k) = a + b\left(\rho(k-m)+\sqrt{(k-m)^2 + \sigma^2}\right)
\tag{1}
\end{equation}\]</span>
<p>onde: <span class="math inline">\(a \in \mathbb R\)</span>, <span class="math inline">\(b \geq 0\)</span>, <span class="math inline">\(|\rho| &lt; 1\)</span>, <span class="math inline">\(m \in \mathbb R\)</span>, <span class="math inline">\(\sigma &gt; 0\)</span>, e <span class="math inline">\(a+b \sigma\sqrt{1 − \rho^2} \geq 0\)</span> para garantir que <span class="math inline">\(\min w(k)&gt;0, \, \forall k \in \mathbb R\)</span>.</p>
</div>
<div id="restricoes-de-nao-arbitragem" class="section level2">
<h2>Restrições de não-arbitragem</h2>
<p>Antes de demonstrar a restrição imposta aos parâmetros <span class="math inline">\(b\)</span> e <span class="math inline">\(\rho\)</span> em função dos limites de inclinação das asas do <em>smile</em>, vamos derivar as expressões para <span class="math inline">\(w\prime(k)\)</span> e <span class="math inline">\(w\prime\prime(k)\)</span> que nos serão úteis na demonstração.</p>
<p>A expressão para <span class="math inline">\(w\prime(k)\)</span> é bastante simples:</p>
<span class="math display" id="eq:wk">\[\begin{equation}
w\prime(k) = b \left[\rho + \frac{(k-m)}{\sqrt{(k-m)^2+\sigma^2}}\right]
\tag{2}
\end{equation}\]</span>
<p>Derivando novamente a equação <a href="#eq:wk">(2)</a> em relação a <span class="math inline">\(k\)</span> teremos uma expressão ainda mais simples, mesmo que após alguma manipulação algébrica um tanto tediosa<a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a>, e resulta em:</p>
<span class="math display" id="eq:wkk">\[\begin{equation}
w\prime\prime(k)=\frac{b\sigma^2}{[(k-m)^2+\sigma^2]^{3/2}}
\tag{3}
\end{equation}\]</span>
<p>onde, se considerarmos <span class="math inline">\(b&gt;0\)</span> temos que <span class="math inline">\(w\prime\prime(k)&gt;0, \,\forall k\in \mathbb R\)</span>, ou seja, o <em>smile</em> de volatilidade definido pela equação <a href="#eq:rawsvi">(1)</a> é <strong>estritamente</strong> convexo.</p>
<p><span class="citation">Rogers and Tehranchi (<a href="#ref-Rogers2010">2010</a>)</span> definiram os limites possíveis para a inclinação das asas em função do tempo para expiração, provando que o <em>smile</em> tende a ficar mais horizontal a medida que o prazo aumenta. Este limite pode ser escrito da seguinte forma e é uma <strong>condição necessária</strong> para a ausência de arbitragem:</p>
<span class="math display" id="eq:rogers">\[\begin{equation}
|w\prime(k)|\leq \frac{4}{\tau} \qquad \forall k \in \mathbb R, \quad \forall \tau \in (0, \infty)
\tag{4}
\end{equation}\]</span>
<p>Sendo o <em>smile</em> convexo, suas máximas inclinações ocorrem quando <span class="math inline">\(k\rightarrow \pm \infty\)</span>. Portanto, deve-se avaliar a restrição dada pela equação <a href="#eq:rogers">(4)</a> nestes limites da seguinte maneira:</p>
<span class="math display">\[\begin{align}
\lim\limits_{k\rightarrow\infty}w\prime(k)&amp;=b(1+\rho)\geq 0\\
\lim\limits_{k\rightarrow-\infty}w\prime(k)&amp;=-b(1-\rho)\leq 0
\end{align}\]</span>
<p>que satisfazendo estas duas relações ao mesmo tempo em que se restringe os parâmetros <span class="math inline">\(b\)</span> e <span class="math inline">\(\rho\)</span> através da inequalidade de Rogers e Tehranchi nos garante o seguinte resultado para um SVI <strong>livre de arbitragem de travas</strong>.</p>
<span class="math display" id="eq:trava">\[\begin{equation}
b(1+|\rho|)\leq\frac{4}{\tau}
\tag{5}
\end{equation}\]</span>
<p>Para garantir que a superfície gerada está livre de arbitragem do tipo borboleta deve-se primeiramente definir uma função<a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a> <span class="math inline">\(g: \mathbb R\rightarrow \mathbb R\)</span>, tal que:</p>
<span class="math display" id="eq:g">\[\begin{equation}
g(k)=\left(1-\frac{kw\prime(k)}{2w(k)}\right)^2-\frac{w\prime(k)^2}{4}\left(\frac{1}{w(k)}+\frac{1}{4}\right)+\frac{w\prime\prime(k)}{2}
\tag{6}
\end{equation}\]</span>
<p>e seguir o lema :</p>
<p><strong>Lema 1</strong> Uma fatia da superfície de volatilidade está livre de arbitragem do tipo borboleta se, e somente se, <span class="math inline">\(g(k) \geq 0\)</span> para todo <span class="math inline">\(k \in \mathbb R\)</span> e <span class="math inline">\(\lim\limits_{k\rightarrow+\infty}d_1(k)=-\infty\)</span>.</p>
<p>Infelizmente, a natureza altamente não-linear da função <span class="math inline">\(g(k)\)</span> impossibilita a derivação de restrições gerais aos parâmetros do SVI. A forma mais simples de eliminar arbitragem do tipo borbobleta é incluir a restrição <span class="math inline">\(g(k) \geq 0\)</span> na função perda e proceder com a calibração dos parâmetros.</p>
</div>
<div id="reparametrizacao-quasi-explicit" class="section level2">
<h2>Reparametrização Quasi-explicit</h2>
<p>Um dos problemas mais marcantes com a calibração do SVI dado pela equação <a href="#eq:rawsvi">(1)</a> é sua natureza altamente não-linear que gera inúmeros pontos de mínimo locais. Mesmo em um ambiente simulado, um típico otimizador de <a href="/post/metodos-de-calibracao">mínimos quadrados</a> como Levenberg-Marquardt não consegue chegar ao mínimo global, onde a função perda é igual a zero. A solução encontrada é dependente dos valores iniciais inputados ao otimizador e a robustez do conjunto de parâmetros encontrados não é garantida.</p>
<p>Uma forma de contornar este problema pode ser a utilização de otimizadores globais, como algortimos genéticos, em um primeiro estágio e então o refinamento desta solução através de um otimizador local (LM, por exemplo).</p>
<p>Outra forma, adotada em <span class="citation">De Marco and Martini (<a href="#ref-DeMarco2009">2009</a>)</span> é a reparametrização da equação <a href="#eq:rawsvi">(1)</a> de forma que esta possa ser tratada como um prolema linear. Para tanto, considere a seguinte troca de variáveis:</p>
<span class="math display">\[\begin{equation}
y = \frac{k-m}{\sigma}
\end{equation}\]</span>
<p>então a parametrização <em>RAW</em> do SVI se torna.</p>
<span class="math display">\[\begin{equation}
w(y) = a + b\sigma\left(\rho y + \sqrt{y^2 + 1}\right)
\end{equation}\]</span>
<p>Definindo agora as seguintes variáveis reparametrizadas é possível reduzir a dimensão de parâmetros de um SVI de 5 para apenas 3:</p>
<span class="math display">\[\begin{align}
c = &amp;b\sigma\\
d = &amp;\rho b \sigma
\end{align}\]</span>
<span class="math display" id="eq:quasiexplicit">\[\begin{equation}
w(y)=a+dy+c\sqrt{y^2+1}
\tag{7}
\end{equation}\]</span>
<p>Portanto, para um par fixo de <span class="math inline">\((m, \sigma)\)</span> nosso problema reduzido é:</p>
<span class="math display" id="eq:reduzido">\[\begin{equation}
P_{m, \sigma}:=\min\limits_{a, c, d \in D}f_y(a, c, d)
\tag{8}
\end{equation}\]</span>
<p>onde <span class="math inline">\(f_y(\cdot)\)</span> é a função objetivo da reparametrização, e é dada pela seguinte equação:</p>
<span class="math display" id="eq:fy">\[\begin{equation}
f_y(a, c, d)=\sum_{i=1}^{n}\left[w(y_i)-\tilde w_i\right]^2
\tag{9}
\end{equation}\]</span>
<p>onde <span class="math inline">\(\tilde w_i\)</span> é a variância total observada correspondente ao <em>moneyness</em> <span class="math inline">\(k_i\)</span>.</p>
<p>O domínio <span class="math inline">\(D\)</span> dos parâmetros <span class="math inline">\(\{a, c, d\}\)</span> é encontrado a partir do limite imposto por <a href="#eq:trava">(5)</a>.</p>
<span class="math display" id="eq:D">\[\begin{equation}
    D =  
    \begin{cases}
    0 \leq c \leq 4\sigma\\
    |d| \leq c \quad \text{e}\quad |d| \leq 4\sigma - c\\
    0 \leq a \leq \max\{\tilde w_i\}\\
    \end{cases}
    \tag{10}
\end{equation}\]</span>
<p>O problema reduzido dado pela equação <a href="#eq:reduzido">(8)</a>, é um típico problema de <strong>mínimos quadrados</strong> com restrições lineares. Este problema, por ser convexo, admite uma única solução interior (se existente) que será o mínimo global para este problema e é encontrada através do gradiente da função objetivo igualando-o a zero, <span class="math inline">\(\nabla f_y = 0\)</span>. Esta equação gera um sistema linear nos parâmetros <span class="math inline">\(a, c, d\)</span> que pode ser explicitamente resolvido. Caso a solução encontrada para este problema esteja contida no domínio <span class="math inline">\(D\)</span>, esta solução é interior e é o mínimo desejado, caso contrário, deve-se percorrer o perímetro do domínio e encontrar o menor valor da função objetivo que será uma solução de canto.</p>
<p>Seja <span class="math inline">\((a^*, c^*, d^*)\)</span> a solução de <a href="#eq:reduzido">(8)</a> e <span class="math inline">\((a^*, b^*, \rho^*)\)</span> os correspondentes parâmetros originais recuperados, então o problema completo de calibração é:</p>
<span class="math display" id="eq:completo">\[\begin{equation}
P:=\min\limits_{m, \sigma}\sum_{i=1}^n (w_*(k_i)-\tilde w_i)^2
\tag{11}
\end{equation}\]</span>
<p>onde <span class="math inline">\(w_*(k)=a^*+b^*\left(\rho^*(k-m)+\sqrt{(k-m)^2 + \sigma^2}\right)\)</span>.</p>
<p>O problema completo, <a href="#eq:completo">(11)</a> é um problema em apenas duas dimensões, <span class="math inline">\((m, \sigma)\)</span> e não-linear, que deve ser abordado através de algum tipo de otimizador global.</p>
<div id="pq" class="section level3">
<h3>Solução explícita do problema reduzido</h3>
<p>Nesta seção apresentaremos a solução, sem considerar as restrições impostas em <a href="#eq:D">(10)</a> para o problema reduzido em <a href="#eq:reduzido">(8)</a>, algo omitido em <span class="citation">De Marco and Martini (<a href="#ref-DeMarco2009">2009</a>)</span>. Esta seção é opcional para o leitor atento que já percebeu a semelhança entre o problema reduzido e um típico problema de regressão linear múltipla.</p>
<p>Para encontrar o conjunto de parâmetros <span class="math inline">\((a^*, c^*, d^*)\)</span> que representam os valores ótimos na equação <a href="#eq:reduzido">(8)</a>, devemos resolver o seguinte sistema de equações:</p>
<span class="math display" id="eq:gradiente">\[\begin{equation}
\nabla f_y = \left[
    \begin{array}{c}
        \partial f_y / \partial a\\ 
        \partial f_y / \partial d\\
        \partial f_y / \partial c
    \end{array} 
\right] = \boldsymbol{0}
\tag{12}
\end{equation}\]</span>
<p>Cada uma das derivadas parciais da equação acima quando igualadas a zero dão origem ao sistema linear apresentado abaixo:</p>
<span class="math display" id="eq:linear">\[\begin{equation}
\scriptsize
\begin{bmatrix}
&amp;n &amp;\sum y_i &amp;\sum\sqrt{y_i^2+1}\\
&amp;\sum y_i &amp;\sum y_i^2 &amp;\sum(y_i\sqrt{y_i^2+1})\\
&amp;\sum\sqrt{y_i^2+1} &amp;\sum(y_i\sqrt{y_i^2+1}) &amp;\sum(y_i^2+1)\\
\end{bmatrix}
\cdot
\begin{bmatrix}
a \\
d \\
c
\end{bmatrix}
=
\begin{bmatrix}
\sum\tilde w_i \\
\sum \tilde w_i y_i \\
\sum(\tilde w_i\sqrt{y_i^2+1})
\end{bmatrix}
\tag{13}
\end{equation}\]</span>
<p>Portanto, o problema reduzido pode ser resolvido através de um sistema linear de ordem 3 sob restrições também lineares.</p>
</div>
</div>
<div id="algoritmo" class="section level2">
<h2>Algoritmo</h2>
<p>A otimização para encontrar os parâmetros ótimos de um SVI dadas observações de mercado e a técnica de calibração <strong>Quasi-explicit</strong> de <span class="citation">De Marco and Martini (<a href="#ref-DeMarco2009">2009</a>)</span> pode ser resumida nos seguintes passos:</p>
<ol style="list-style-type: decimal">
<li><p>Definir valores iniciais para os parâmetros <span class="math inline">\((m, \sigma)\)</span>,</p></li>
<li><p>Iniciar algum otimizador global com estes parâmetros e resolver o problema completo <a href="#eq:completo">(11)</a></p>
<p>2.1 Dentro da otimização global, resolver o problema reduzido <a href="#eq:reduzido">(8)</a> para os parâmetros <span class="math inline">\((m, \sigma)\)</span> dados,</p></li>
<li><p>Na convergência do problema completo do passo 2, otimizar uma última vez o problema reduzido,</p></li>
<li><p>Recuperar os parâmetros <span class="math inline">\((a, b, \rho, m, \sigma)\)</span></p></li>
</ol>
<p>A escolha dos otimizadores fica a cargo pessoal, sendo sugerido testar vários para o mesmo problema. Eventualmente, para um determinado <em>smile</em> um otimizador pode se mostrar melhor que outro que vinha sendo utilizado em outras ocasiões.</p>
<p>Nos testes realizados pelo <a href="http://clubedefinancas.com.br">Clube de Finanças</a>, entre os otimizadores globais para o problema completo utilizamos <a href="https://cran.r-project.org/package=GA"><strong>Algoritmos Genéticos</strong></a>, <a href="https://www.rdocumentation.org/packages/stats/versions/3.5.2/topics/constrOptim"><strong>Nelder-Mead restrito</strong></a> e um <a href="https://cran.r-project.org/package=Rsolnp"><strong>Método não-linear generalizado</strong></a>. Para o problema reduzido, apesar de ser linear, o método de Nelder-Mead restrito se mostrou tão eficiente quanto e de mais fácil implementação. Se o objetivo for fazer uma calibração direta, dos cinco parâmetros ao mesmo tempo, uma combinação de otimizador global em primeiro estágio e o método de Levenberg-Marquardt restrito para refinamento da solução é o ideal.</p>
</div>
<div id="resultados" class="section level2">
<h2>Resultados</h2>
<p>A seguir apresentamos um <em>smile</em> de referência para a calibração, obtido de <a href="http://www.ivolatility.com/doc/usa/IV_Raw_Delta_surface.csv">ivolatility.com</a> e então partimos para diferentes técnicas de calibração de um SVI. Os códigos em <a href="https://cran.r-project.org/">R</a> também estão apresentados ao longo do texto para melhor compreensão e estudo do leitor.</p>
<p>Os dados utilizados neste exemplo estão apresentados na tabela <a href="#tab:dados">1</a> abaixo. Esta é uma típica apresentação de um <em>slice</em> de superfície, ou seja, dados para um <em>smile</em> apenas. As principais variáveis são: a data em que os dados foram coletados (date), o preço de fechamento do ativo (stock_price), o prazo para expiração em dias (period), e medidas de moneyness como delta, <em>strike</em> e o próprio <em>moneyness</em>, além é claro da volatilidade implícita (iv) retirada do mercado.</p>
<table class="table table-striped" style="font-size: 14px; width: auto !important; margin-left: auto; margin-right: auto;">
<caption style="font-size: initial !important;">
<span id="tab:dados">Tabela 1: </span>Dados reais para exemplo de calibração de uma SVI.
</caption>
<thead>
<tr>
<th style="text-align:left;">
date
</th>
<th style="text-align:left;">
symbol
</th>
<th style="text-align:left;">
exchange
</th>
<th style="text-align:right;">
stock_price_for_iv
</th>
<th style="text-align:right;">
period
</th>
<th style="text-align:right;">
delta
</th>
<th style="text-align:right;">
moneyness
</th>
<th style="text-align:right;">
strike
</th>
<th style="text-align:right;">
iv
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
0.03
</td>
<td style="text-align:right;">
148.41
</td>
<td style="text-align:right;">
0.09
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
15
</td>
<td style="text-align:right;">
0.03
</td>
<td style="text-align:right;">
147.49
</td>
<td style="text-align:right;">
0.09
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
20
</td>
<td style="text-align:right;">
0.02
</td>
<td style="text-align:right;">
146.80
</td>
<td style="text-align:right;">
0.09
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
25
</td>
<td style="text-align:right;">
0.02
</td>
<td style="text-align:right;">
146.21
</td>
<td style="text-align:right;">
0.09
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
0.01
</td>
<td style="text-align:right;">
145.69
</td>
<td style="text-align:right;">
0.09
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
35
</td>
<td style="text-align:right;">
0.01
</td>
<td style="text-align:right;">
145.19
</td>
<td style="text-align:right;">
0.10
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
40
</td>
<td style="text-align:right;">
0.01
</td>
<td style="text-align:right;">
144.69
</td>
<td style="text-align:right;">
0.10
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
45
</td>
<td style="text-align:right;">
0.00
</td>
<td style="text-align:right;">
144.18
</td>
<td style="text-align:right;">
0.10
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
50
</td>
<td style="text-align:right;">
0.00
</td>
<td style="text-align:right;">
143.66
</td>
<td style="text-align:right;">
0.10
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
55
</td>
<td style="text-align:right;">
0.00
</td>
<td style="text-align:right;">
143.12
</td>
<td style="text-align:right;">
0.11
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
60
</td>
<td style="text-align:right;">
-0.01
</td>
<td style="text-align:right;">
142.53
</td>
<td style="text-align:right;">
0.11
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
65
</td>
<td style="text-align:right;">
-0.01
</td>
<td style="text-align:right;">
141.88
</td>
<td style="text-align:right;">
0.11
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
70
</td>
<td style="text-align:right;">
-0.02
</td>
<td style="text-align:right;">
141.13
</td>
<td style="text-align:right;">
0.12
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
75
</td>
<td style="text-align:right;">
-0.02
</td>
<td style="text-align:right;">
140.26
</td>
<td style="text-align:right;">
0.13
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
80
</td>
<td style="text-align:right;">
-0.03
</td>
<td style="text-align:right;">
139.16
</td>
<td style="text-align:right;">
0.13
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
85
</td>
<td style="text-align:right;">
-0.04
</td>
<td style="text-align:right;">
137.66
</td>
<td style="text-align:right;">
0.14
</td>
</tr>
<tr>
<td style="text-align:left;">
2017-09-21
</td>
<td style="text-align:left;">
IWM
</td>
<td style="text-align:left;">
NYSEArca
</td>
<td style="text-align:right;">
143.73
</td>
<td style="text-align:right;">
30
</td>
<td style="text-align:right;">
90
</td>
<td style="text-align:right;">
-0.06
</td>
<td style="text-align:right;">
135.32
</td>
<td style="text-align:right;">
0.16
</td>
</tr>
</tbody>
</table>
<p>Esta tabela poderia conter (de fato contém no arquivo original) outros períodos de expiração, e neste caso uma das colunas de <em>moneyness</em> começa a se repetir, no caso seria o delta pois baixamos uma tabela de volatilidades implícitas por delta. Assim, em uma tabela simples em formato <a href="http://vita.had.co.nz/papers/tidy-data.html"><code>tidy</code></a> é possível armazenar informações de uma superfície inteira, a qual de outra forma necessitaria de um arranjo em 3 dimensões.</p>
<p>Ressaltamos aqui que a unidade de volatilidade implícita está em percentuais <strong>ao ano</strong>, equanto que nosso período é de dias corridos. É necessário harmonizar estas medidas de forma que, para volatiliades dadas em percentual ao ano, o período também seja dado em <strong>anos</strong>. Logo nosso <span class="math inline">\(\tau = 30/365\)</span>, ou seja, 0.08219.</p>
<p>Demonstraremos aqui os resultados para a calibração de uma <em>RAW SVI</em> pelos métodos “Direto”, “GA”, “Quasi-NM” e “Quasi-PQ”, abaixo explicados.</p>
<p>O método “Direto” é uma calibração direta através de um algoritmo de <strong>Levenberg-Marquardt</strong> da equação <a href="#eq:rawsvi">(1)</a>, ou seja, não existe reparametrização <em>Quasi-explicit</em> e o problema resolvido é não-linear em 5 dimensões. São realizadas 10 calibrações com estimativas iniciais dos parâmetros aleatórias, mas dentro de seus respectivos domínios. A melhor solução, aquela com o menor valor para a função objetivo, é selecionada. Todos os outros métodos utilizam a reparametrização, ocorrendo variações apenas nos algoritmos de otimização utilizados nos problemas reduzido e completo.</p>
<p>A calibração “GA” faz uso do otimizador global de <strong>algoritmos genéticos</strong> para o problema completo, ou seja, para estimar o par <span class="math inline">\((m, \sigma)\)</span> que corresponde ao mínimo global. Após, o problema reduzido é resolvido através do algoritmo de <strong>Nelder-Mead</strong>. Este método é robusto, pois o algoritmo genético tem grande probabilidade de encontrar a região onde se encontra o mínimo global e não ficar preso localmente. Entretanto a robustez ocorre as expensas do tempo de computação.</p>
<p>Os métodos ditos “Quasi” diferem entre si na resolução do problema reduzido. Enquanto “PQ” remete a <strong>programação quadrática</strong> e faz uso da resolução do sistema linear apresentado na equação <a href="#eq:linear">(13)</a> com as restrições impostas por <a href="#eq:D">(10)</a>, o método “Quasi-NM” utiliza o método de <strong>Nelder-Mead</strong> com restrições para a resolução deste mesmo problema reduzido. Em ambos os métodos, o problema completo é resolvido com um algoritmo de Nelder-Mead com 50 reinicializações das estimativas iniciais dos parâmetros <span class="math inline">\((m, \sigma)\)</span>, o que causa algum impacto no tempo de computação destes métodos.</p>
<pre class="r"><code>smile &lt;- dados %&gt;% 
  mutate(tau = period / 365) %&gt;% 
  select(moneyness, iv, tau)

par_names &lt;- factor(c(&quot;a&quot;, &quot;b&quot;, &quot;$\\rho$&quot;, &quot;m&quot;, &quot;$\\sigma$&quot;),
                    levels = c(&quot;a&quot;, &quot;b&quot;, &quot;$\\rho$&quot;, &quot;m&quot;, &quot;$\\sigma$&quot;))
k &lt;- smile$moneyness
w &lt;- smile$iv^2 * smile$tau

init_direct &lt;- proc.time()
par_direct &lt;- svi_fit_direct(k, w)
end_direct &lt;- proc.time()
time_direct &lt;- end_direct - init_direct 

init_ga &lt;- proc.time()
par_ga &lt;- svi_fit_ga(k, w)
end_ga &lt;- proc.time()
time_ga &lt;- end_ga - init_ga 

init_quasipq &lt;- proc.time()
par_quasipq &lt;- svi_fit_quasi(k, w, inner = &quot;quadprog&quot;)
end_quasipq &lt;- proc.time()
time_quasipq &lt;- end_quasipq - init_quasipq 

init_quasinm &lt;- proc.time()
par_quasinm &lt;- svi_fit_quasi(k, w)
end_quasinm &lt;- proc.time()
time_quasinm &lt;- end_quasinm - init_quasinm 

iv_direct &lt;- sqrt(svi_fun(par_direct$par[[1]], k) / smile$tau)
iv_ga &lt;- sqrt(svi_fun(par_ga$par[[1]], k) / smile$tau)
iv_quasipq &lt;- sqrt(svi_fun(par_quasipq$par[[1]], k) / smile$tau)
iv_quasinm &lt;- sqrt(svi_fun(par_quasinm$par[[1]], k) / smile$tau)

plot_tbl &lt;- tibble(k = k,
              Direct = iv_direct,
              GA = iv_ga,
              QuasiPQ = iv_quasipq,
              QuasiNM = iv_quasinm,
              observed = smile$iv) %&gt;% 
  gather(key = method, value = iv, -c(k, observed))

par_tbl &lt;- bind_rows(par_direct, par_ga, par_quasipq, par_quasinm) %&gt;% 
  select(method, par) %&gt;% 
  unnest() %&gt;% 
  mutate(names = rep(par_names, 4)) %&gt;% 
  spread(method, par) %&gt;% 
  select(names, Direct, GA, QuasiPQ, QuasiNM) %&gt;% 
  mutate(names = as.character(names)) %&gt;% 
  mutate_at(vars(Direct:QuasiNM), arred)

rmse_tbl &lt;- bind_rows(par_direct, par_ga, par_quasipq, par_quasinm) %&gt;% 
  select(method, par) %&gt;% 
  unnest() %&gt;% 
  group_by(method) %&gt;% 
  summarise(RMSE = rmse(par, k, w)) %&gt;% 
  spread(method, RMSE) %&gt;% 
  mutate(names = &quot;RMSE&quot;) %&gt;% 
  select(names, Direct, GA, QuasiPQ, QuasiNM) %&gt;% 
  mutate_at(vars(Direct:QuasiNM), format, digits = 3, scientific = TRUE)

time_tbl &lt;- tibble(method = c(&quot;Direct&quot;, &quot;GA&quot;, &quot;QuasiPQ&quot;, &quot;QuasiNM&quot;),
                   time = rbind(time_direct, time_ga, 
                                time_quasipq, time_quasinm)[, 3]) %&gt;% 
  spread(method, time) %&gt;% 
  mutate(names = &quot;Tempo&quot;) %&gt;% 
  select(names, Direct, GA, QuasiPQ, QuasiNM) %&gt;% 
  mutate_at(vars(Direct:QuasiNM), arred)

frame_tbl &lt;- bind_rows(par_tbl, rmse_tbl, time_tbl)</code></pre>
<p>Abaixo é apresetanda uma tabela com os valores estimados para os parâmetros da SVI, o RMSE (root mean square error) e o tempo total em segundos para a calibração. Aqui o RMSE é definido como <span class="math inline">\(\sqrt{1/n\sum(w(k_i)-\tilde w_i)^2}\)</span> e nos fornece um valor típico de erro <strong>na variância</strong>.</p>
<pre class="r"><code>kable(frame_tbl,
      col.names = c(&quot;Estimativa&quot;, &quot;Direto&quot;, &quot;GA&quot;, 
                    &quot;QuasiPQ&quot;, &quot;QuasiNM&quot;),
      caption = &quot;Parâmetros estimados da calibração, RMSE e tempo de computação em segundos.&quot;) %&gt;% 
  kable_styling(bootstrap_options = &quot;striped&quot;,
                font_size = 20,
                full_width = FALSE)</code></pre>
<table class="table table-striped" style="font-size: 20px; width: auto !important; margin-left: auto; margin-right: auto;">
<caption style="font-size: initial !important;">
<span id="tab:tabela">Tabela 2: </span>Parâmetros estimados da calibração, RMSE e tempo de computação em segundos.
</caption>
<thead>
<tr>
<th style="text-align:left;">
Estimativa
</th>
<th style="text-align:left;">
Direto
</th>
<th style="text-align:left;">
GA
</th>
<th style="text-align:left;">
QuasiPQ
</th>
<th style="text-align:left;">
QuasiNM
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;">
a
</td>
<td style="text-align:left;">
0.00000
</td>
<td style="text-align:left;">
0.00000
</td>
<td style="text-align:left;">
0.00000
</td>
<td style="text-align:left;">
0.00011
</td>
</tr>
<tr>
<td style="text-align:left;">
b
</td>
<td style="text-align:left;">
0.01964
</td>
<td style="text-align:left;">
0.01952
</td>
<td style="text-align:left;">
0.01651
</td>
<td style="text-align:left;">
0.01870
</td>
</tr>
<tr>
<td style="text-align:left;">
<span class="math inline">\(\rho\)</span>
</td>
<td style="text-align:left;">
-0.81157
</td>
<td style="text-align:left;">
-0.80220
</td>
<td style="text-align:left;">
-1.00000
</td>
<td style="text-align:left;">
-0.90090
</td>
</tr>
<tr>
<td style="text-align:left;">
m
</td>
<td style="text-align:left;">
-0.00861
</td>
<td style="text-align:left;">
-0.00773
</td>
<td style="text-align:left;">
-0.01131
</td>
<td style="text-align:left;">
-0.01145
</td>
</tr>
<tr>
<td style="text-align:left;">
<span class="math inline">\(\sigma\)</span>
</td>
<td style="text-align:left;">
0.05101
</td>
<td style="text-align:left;">
0.05039
</td>
<td style="text-align:left;">
0.07100
</td>
<td style="text-align:left;">
0.05027
</td>
</tr>
<tr>
<td style="text-align:left;">
RMSE
</td>
<td style="text-align:left;">
8.71e-06
</td>
<td style="text-align:left;">
8.68e-06
</td>
<td style="text-align:left;">
9.62e-05
</td>
<td style="text-align:left;">
1.02e-05
</td>
</tr>
<tr>
<td style="text-align:left;">
Tempo
</td>
<td style="text-align:left;">
0.11800
</td>
<td style="text-align:left;">
24.93800
</td>
<td style="text-align:left;">
0.14100
</td>
<td style="text-align:left;">
5.94600
</td>
</tr>
</tbody>
</table>
<p>O método Direto, com algoritmo de Levenberg-Marquardt se mostrou muito mais rápido que os demais, principalmente com relação ao algoritmo genético, e com um bom ajuste dado o baixo valor de RMSE. O algoritmo genético é consideravelmente mais lento, entretanto durante as várias calibrações realizadas em testes (e que não estão apresentadas na tabela <a href="#tab:tabela">2</a>), este algoritmo sempre se mostrou robusto, com baixo RMSE, diferentemente dos outros métodos que por vezes, denpendendo das estimativas iniciais, podem convergir para um mínimo local.</p>
<p>O gráfico com os ajustes realizados pode ser observado abaixo.</p>

<pre class="r"><code>ggplot(plot_tbl, aes(x = k)) + 
  geom_point(aes(y = observed)) +
  geom_line(aes(y = iv, color = method)) +
  guides(color = guide_legend(title = &quot;&quot;)) +
  labs(title = &quot;&quot;,
       x = &quot;Forward log-moneyness&quot;,
       y = &quot;Volatility&quot;,
       caption = &quot;&quot;) +
  scale_y_continuous(labels = scales::percent) +
  scale_color_viridis_d() +
  theme_economist_white()</code></pre>
<div class="figure"><span id="fig:plot"></span>
<img src="/post/2019-02-15-calibrando-uma-svi_files/figure-html/plot-1.png" alt="Comparação entre diferentes métodos de calibração de uma SVI." width="672" />
<p class="caption">
Figura 1: Comparação entre diferentes métodos de calibração de uma SVI.
</p>
</div>
<p>De fato o método direto e o método <em>Quasi-explicit</em> com otimizador global do tipo algoritmo genético se mostram mais adequados para a calibração de um SVI. Enquanto o método direto é muito mais eficiente em termos computacionais, o método <em>Quasi-explicit</em> com GA é mais robusto. Desta forma, deve-se salientar que é necessário que o usuário, ao fazer uma calibração de <em>smile</em> de volatilidade, deve dispor de diferentes métodos de fazê-lo, e a inspeção visual do resultado é <strong>obrigatória</strong> para determinar qual método foi mais eficiente em ajustar a curva aos dados.</p>
</div>
<div id="conclusao" class="section level2">
<h2>Conclusão</h2>
<p>Apesar de neste exemplo ter se mostrado um método efetivo, com bom ajuste e baixo tempo de calibração, o método direto é altamente dependente dos valores iniciais dos parâmetros. Para tornar este método mais robusto, um número maior de reinicializações deve ser feita o que penaliza o tempo de calibração. O método <em>Quasi-explicit</em> com algoritmo genético para encontrar a região de <span class="math inline">\((m, \sigma)\)</span> onde se encontra o mínimo global se mostrou bastante robusta, entretanto, de convergência lenta. Para ajustar apenas um <em>smile</em> alguns segundos a mais não representam problema. Porém, se imaginarmos que em uma grande instituição financeira são necessárias calibrações de, talvez, milhares de <em>smiles</em> representando inúmeras superfícies de diversos instrumentos, este método pode se mostrar computacionalmente caro.</p>
<p>Já os métodos <em>Quasi-explicit</em> que utilizam um algoritmo de Nelder-Mead para a resolução do problema completo se mostraram muito sensíveis às estimativas iniciais dos parâmetros. Mesmo utilizando 50 reinicialzações do método, diversas vezes o ajuste realizado foi insatisfatório. A resolução através de programação quadrática é rápida, se comparada com NM, entretanto, quando as restrições impostas pela equação <a href="#eq:D">(10)</a> se tornam ativas, este método parece sofrer com algum viés em sua solução.</p>
</div>
<div id="referencias" class="section level2 unnumbered">
<h2>Referências</h2>
<div id="refs" class="references">
<div id="ref-DeMarco2009">
<p>De Marco, S, and C Martini. 2009. “Quasi-Explicit Calibration of Gatheral’s Svi Model’.” <em>Zeliade White Paper</em>, 1–15.</p>
</div>
<div id="ref-Gatheral2004">
<p>Gatheral, Jim. 2004. “A Parsimonious Arbitrage-Free Implied Volatility Parameterization with Application to the Valuation of Volatility Derivatives.” <em>Presentation at Global Derivatives &amp; Risk Management, Madrid</em>.</p>
</div>
<div id="ref-Gatheral2014">
<p>Gatheral, Jim, and Antoine Jacquier. 2014. “Arbitrage-Free Svi Volatility Surfaces.” <em>Quantitative Finance</em> 14 (1). Taylor &amp; Francis: 59–71.</p>
</div>
<div id="ref-Lee2004">
<p>Lee, Roger W. 2004. “The Moment Formula for Implied Volatility at Extreme Strikes.” <em>Mathematical Finance: An International Journal of Mathematics, Statistics and Financial Economics</em> 14 (3). Wiley Online Library: 469–80.</p>
</div>
<div id="ref-Rogers2010">
<p>Rogers, Leonard CG, and MR Tehranchi. 2010. “Can the Implied Volatility Surface Move by Parallel Shifts?” <em>Finance and Stochastics</em> 14 (2). Springer: 235–48.</p>
</div>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>A resolução desta derivada é uma simples regra da divisão, entretanto a simplificação do resultado pede alguma manipulação algébrica. É possível utilizar sistemas de computação simbólica, o qual recomendamos o <a href="https://live.sympy.org/">SymPy</a><a href="#fnref1">↩</a></p></li>
<li id="fn2"><p>Condições para ausência de arbitragem do tipo borboleta em um SVI estão detalhadas na seção 2.2 do artigo de <span class="citation">Gatheral and Jacquier (<a href="#ref-Gatheral2014">2014</a>)</span>.<a href="#fnref2">↩</a></p></li>
</ol>
</div>
