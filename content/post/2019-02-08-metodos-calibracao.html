---
title: Métodos de calibração de superfícies de volatilidade
author: Rafael Bressan
date: '2019-02-08'
categories:
  - Derivativos
tags:
  - opções
  - Quant
  - R
  - SVI
  - Volatilidade
slug: metodos-de-calibracao-de-superficies-de-volatilidade
aliases: /post/metodos-de-calibracao
bibliography: library.bib
link-citations: true
---



<p>Neste post iremos mostrar as diferenças existentes entre “interpolação”, “suavização” e “parametrização” de superfícies de volatilidade.</p>
<p>Como já apresentado em posts anteriores, existem diversas formas de interpolar, extrapolar, parametrizar e calibrar smiles de volatilidade. Exsitem vantagens e desvantagens para cada método.</p>
<div id="calibracao-versus-interpolacao" class="section level2">
<h2>Calibração versus Interpolação</h2>
<p>Uma forma simples de gerar um smile de volatilidade a partir de dados observados no mercado é a <strong>interpolação</strong> destes dados. Diversas formas de interpolação existem, sendo talvez a mais conhecida a spline cúbica. Não é a proposta deste artigo detalhar os procedimentos de interpolação, restando saber que em tal procedimento é gerada uma função contínua em partes (piecewise) que <strong>passa</strong> por todos os pontos observados.</p>
<p>Uma interpolação força a passagem da função interpolada em todos os seus pontos de referência, como se estivesse ligando os pontos em um desenho a mão livre. Portanto, nestes pontos o erro da interpolação é zero por definição, entretanto em pontos intermediários podem surgir erros, inclusive aqueles que possibilitam a existência de arbitragem entre strikes de um mesmo smile<a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a>.</p>
<p>Em contraposição a métodos de interpolação, podem ser derivados métodos de suavização (smoothing) ou então a parametrização do smile de volatilidade. Seja suavização, ou parametrização, estes métodos não forçam a passagem da função que representa o smile pelos pontos de mercado, mas buscam minimizar alguma função perda com relação aos desvios em relação a estes pontos ao mesmo tempo em que buscam “suavizar” o smile, para que este não apresente variações bruscas entre os strikes ou alterações de convexidade na curva de preços, que não são condizentes com a teoria de precificação de derivativos.</p>
<p>Um método paramétrico, como o SVI, Heston, SABR ou Volatilidade Local, busca ajustar às volatilidades implícitas observadas através dos preços das opções sendo praticados no mercado uma determinada função, que possui parâmetros em sua definição que por sua vez determinam a forma desta função. Ao se ajustar os parâmetros, pode-se adequar a função para ficar “o mais próxima possível” dos dados observados, sem necessariamente, no entanto, passar por todos estes pontos.</p>
<p>A figura abaixo tenta mostrar as diferenças entre uma interpolação spline cúbica, uma suavização e uma parametrização SVI. Enquanto que a interpolação liga todos os pontos marcados, a suavização e a parametrização não necessariamente passam sobre estes pontos mas fornecem uma curva mais “suave”, sem trocas de convexidade, o que gera oportunidades de arbitragem e probabilidades negativas de ocorrência de determinados preços para o ativo subjacente, que ferem os princípios de precificação de opções. Os dados utilizados neste e nos próximos artigos sobre superfícies de volatlidade foram obtidos do site <a href="http://www.ivolatility.com/doc/usa/IV_Raw_Delta_surface.csv">ivolatility.com</a> na forma de amostra gratuita fornecida livremente. O ativo subjacente é o ETF <a href="https://www.ishares.com/us/products/239710/ishares-russell-2000-etf"><code>IWM</code></a> para a data de 21/09/2017.</p>
<div class="figure"><span id="fig:diferencas"></span>
<img src="/post/2019-02-08-metodos-calibracao_files/figure-html/diferencas-1.png" alt="Diferentes métodos de ajuste de dados a um smile." width="672" />
<p class="caption">
Figura 1: Diferentes métodos de ajuste de dados a um smile.
</p>
</div>
<p>Pode-se verificar como os métodos SVI e a suavização não passam sobre todos os pontos marcados, com a suavização tendo dificuldade com a curvatura nos valores mais altos de <em>moneyness</em> e a SVI possuindo uma inclinação mais branda na asa esquerda do smile.</p>
</div>
<div id="spline-cubica" class="section level2">
<h2>Spline cúbica</h2>
<p>Este método é possivelmente um dos mais flexíveis e conhecidos de interpolação de dados univariados existente, embora também exista sua versão bi-dimensional. Uma spline nada mais é que “uma curva definida matematicamente por dois ou mais pontos de controle”<a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a>.</p>
<p>No caso da spline cúbica, esta é uma função polinomial de grau 3 definida em cada subintervalo demarcados pelos pontos de controle, no caso de interpolação são todos nós. Ou seja, considere um segmento entre dois pontos consecutivos <span class="math inline">\([c, d]\in S\)</span> a spline é uma função cúbica com seus parâmetros calculados pelo algoritmo de ajuste. Para o próximo intervalo de pontos dentro do domínio da função, um novo polinômio de grau 3 é ajustado, sendo que nos pontos de nós uma restrição de igualdade entre as derivadas nos dois segmentos é aplicada para garantir a suavidade da função interpolada como um todo.</p>
<p>Assim, uma spline cúbica é uma função contínua, suave e diferenciável até a segunda ordem. Entretanto, suas derivadas, apesar de contínuas, podem não ser suaves, especialmente aquela de segunda ordem que pode apresentar pontos de “ruptura”. Esta característica de uma spline cúbica a torna pouco atrativa para a inferência de distribuições de probabilidade a partir de dados de volatilidade ou mesmo dos preços de opções.</p>
<div class="figure"><span id="fig:cubic-spline"></span>
<img src="/images/cubic_spline.png" alt="Cada segmento de uma spline cúbica é um polinômio de grau 3 diferente."  />
<p class="caption">
Figura 2: Cada segmento de uma spline cúbica é um polinômio de grau 3 diferente.
</p>
</div>
</div>
<div id="suavizacao" class="section level2">
<h2>Suavização</h2>
<p>A técnica de suavização é muito semelhante a interpolação, inclusive o método spline também é aplicado, com algumas modificações de forma que nem todos os pontos fornecidos serão nós.</p>
<p>Na spline de suavização (ou aproximação), os pontos fornecidos são separados entre os nós, onde a função deve passar e pontos de controle, que são utilizados para controlar a curvatura da função nestes pontos.</p>
<p>Estas suavizações são principalmente utilizadas quando se possui muitas observações sujeitas a ruídos, de forma que uma interpolação entre todos os pontos seria tanto impraticável quanto sem sentido. O que se deseja, portanto, é uma função <strong>aproximada</strong> que melhor descreva o processo sob análise.</p>
<p>Um ponto em comum entre estas técnicas é o parâmetro de suavização, ausente, na interpolação, que controla a “suavidade” da função estimada.</p>
<div class="figure"><span id="fig:suavizacao"></span>
<img src="/post/2019-02-08-metodos-calibracao_files/figure-html/suavizacao-1.png" alt="Menor parâmetro de suavização gera granularidade na curva." width="672" />
<p class="caption">
Figura 3: Menor parâmetro de suavização gera granularidade na curva.
</p>
</div>
</div>
<div id="parametrizacao" class="section level2">
<h2>Parametrização</h2>
<p>E por fim as técnicas de parametrização. Nesta categoria estão diversos conhecidos modelos de superfícies de volatilidade implícita, dentre eles os modelos de <span class="citation">Heston (<a href="#ref-Heston1993">1993</a>)</span>, Volatilidade Local de <span class="citation">Dupire (<a href="#ref-Dupire1994">1994</a>)</span> e SVI de <span class="citation">Gatheral (<a href="#ref-Gatheral2004">2004</a>)</span>.</p>
<p>Em comum, estes modelos tentam parametrizar a superfície, e por conseguinte o smile de volatilidade, de acordo com alguma função, em geral não-linear, que possui características condizentes com a teoria de precificão de derivativos e também a observação empírica das superfícies.</p>
<p>Por exemplo, a parametrização <em>raw</em> da SVI possui a seguinte forma para a <strong>variância total</strong><a href="#fn3" class="footnoteRef" id="fnref3"><sup>3</sup></a> :</p>
<p><span class="math display">\[ w(k) = a + b\left(\rho(k-m)+\sqrt{(k-m)^2 + \sigma^2}\right)\]</span></p>
<p>que fornece um espaço de cinco parâmetros <span class="math inline">\(\chi_B=\{a, b, \rho, m, \sigma\}\)</span> que definem o smile e devem, portanto, serem calibrados a partir de dados observados no mercado.</p>
<p>O procedimento de calibração consiste em encontrar o conjunto de parâmetros que minimizam uma função perda entre a volatilidade prevista pelo modelo e os dados de mercado, enquanto satisfazem algumas restrições adicionais, como “ausência de arbitragem”, suavidade, etc. Trata-se, via de regra, de problemas de otimização não-linear com restrições de inequalidade também não-lineares.</p>
<div id="funcao-perda" class="section level3">
<h3>Função perda</h3>
<p>A função perda, ou função de calibração pode ser definida de diversas maneiras, de forma geral, para uma determinada maturidade, ela toma a forma:</p>
<p><span class="math display">\[L=\sum\limits_{i=1}^n\lambda_i||\hat w(k_i)-w_{imp}(k_i)||\]</span> onde <span class="math inline">\(||\cdot||\)</span> é alguma medida de norma, sendo a mais conhecida o quadrado das diferenças, dando origem a minimização do erro quadrático médio (RMSE). Para este smile sendo calibrado existem <span class="math inline">\(n\)</span> strikes (<span class="math inline">\(k_i\)</span>) e suas volatilidades implícitas observadas são <span class="math inline">\(w_{imp}(k_i)\)</span>. A resposta do modelo para um determinado strike é <span class="math inline">\(\hat w(k_i)\)</span> e <span class="math inline">\(\lambda_i\)</span> são os pesos dados na função perda para cada um destes strikes.</p>
<p>Os pesos <span class="math inline">\(\lambda_i\)</span> são utilizados para ponderar as observações das volatilidades mais importantes para o cálculo, onde se deseja que a curva ajustada possua um menor erro. Em geral, estes pesos são calculado como inversamente proporcionais:</p>
<ul>
<li>ao quadrado dos <em>spreads bid-ask</em>, para dar mais importância às opções mais líquidas</li>
<li>ao quadrado da grega vega calculada a partir do modelo BSM</li>
</ul>
</div>
<div id="otimizadores" class="section level3">
<h3>Otimizadores</h3>
<p>Os otimizadores são os algoritmos pelos quais o problema de minimização posto é resolvido. Se a função perda é convexa, e ela deve ser construída de forma a ser, mesmo que não estritamente, então ela possui um ou mais pontos de mínimo onde o gradiente desta função é igual a zero. O que os otimizadores fazem é buscar o conjunto de parâmetros que minimizam a função perda e atendem as restrições impostas simultaneamente. Os otimizadores podem ser classificados em dois grandes grupos, globais e locais.</p>
<p>Algoritmos locais dependem de uma estimativa inicial dos parâmetros para começarem a busca pelo mínimo. Seguindo uma regra utilizando-se o gradiente da função ou alguma heurística, estes otimizadores caminham em direção ao ponto de mínimo mais próximo da estimativa inicial, daí o nome “local”. Como desvantagem destes otimizadores é a mais evidente é que se a função perda for altamente não-linear, com diversos pontos de mínimo local, este otimizador pode ficar preso em um destes pontos sem nunca, no entanto, encontrar o mínimo global. Eles são, portanto muito sensíveis à estimativa inicial dos parâmetros.</p>
<p>Por sua vez, otimizadores globais buscam mapear todo o espaço factível para os parâmetros e encontrar o ponto mínimo da função perda dentro deste espaço. Estes algoritmos não dependem de estimativas iniciais, uma vez que tentarão avaliar o espaço completo. São utilizados quando o problema de minimização é não-linear e possui múltiplos pontos de mínimo local. Estes algoritmos usam alguma forma de heurística para encontrar a região onde o mínimo global está localizado, mas são, em geral, ineficientes em apontar rapidamente onde este ponto de mínimo se encontra com precisão. Por esta razão, é frequente a utilização de otimizadores globais com um posterior refinamento de sua solução por algum algoritmo local.</p>
<p>Abaixo apresentamos alguns exemplos mais comuns de otimizadores, tanto locais quanto globais:</p>
<ul>
<li><p><strong>Gauss-Newton</strong>: Este método é utilizado para encontrar as raízes de alguma função. Para encontrar o ponto de mínimo da função perda, precisa-se encontrar as raízes do gradiente desta função, portanto o método de Newton em otimização faz uso da função gradiente. Este é um método de otimização local.</p></li>
<li><p><strong>Levenberg-Marquardt</strong>: Método muito utilizado para problemas não-lineares, ele parte de uma modificação ao método de Gauss-Newton ao introduzir um fator de amortecimento calculado iterativamente.</p></li>
<li><p><strong>L-BFGS-B</strong>: BFGS é um método conhecido como quasi-Newton, onde não é necessário calcular a Hessiana do problema, ela é aproximada a partir do próprio gradiente. É bastante utilizado para resolver problemas não-lineares e em sua versão L-BFGS-B pode lidar com restrições do tipo <em>box</em>, intervalo dos parâmetros é fixo.</p></li>
<li><p><strong>Nelder-Mead</strong>: Este é um método livre do uso de gradiente, já que usa uma heurística para construir um simplex e a partir deste “procurar” por um mínimo. Bastante utilizado quando a função objetivo pode não ser diferenciável. Faz uso de um simplex inicial, que pode ser grande o suficiente para encampar o mínimo global, entretanto, não se classifica como um otimizador global.</p></li>
<li><p><strong>Algoritmo Genético</strong>: Este método utiliza conceitos da seleção natural para gerar os resultados da otimização. É um otimizador global, no sentido que independe de uma estimativa inicial de parâmetros e faz uma busca por todo o espaço factível. Em um algoritmo genético, uma população aleatória inicial de parâmetros é criada e a partir desta, as gerações evoluem conforme mutações e <em>cross-over</em> de características e é avaliado o <em>fitness</em> de cada conjunto de parâmetros até um deles ser considerado adequado.</p></li>
<li><p><strong>Evolução Diferencial</strong>: É um método de otimização global, assim como o Algoritmo Genético e o Enxame de Partículas. Sua diferença reside no fato de que sua população inicial é constantemente avaliada e deslocada de posição. Se o agente obtiver uma situação melhor (menor valor para a função perda) na nova posição, esta agora faz parte da população. Desta forma os agentes, antes espalhados pelo espaço factível dos parâmetros, tendem a convergir para um ponto com o menor valor da função perda.</p></li>
<li><p><strong>Enxame de Partículas</strong>: Do inglês, <em>Particle Swarm Optimization - PSO</em> este método é semelhante ao DE <em>(Differential Evolution)</em> porém as partículas (o equivalente dos agentes no DE) matém informações sobre a posição da melhor partícula até então, de forma a fazer com que as partículas tendam para a melhor solução.</p></li>
</ul>
</div>
</div>
<div id="conclusao" class="section level2">
<h2>Conclusão</h2>
<p>Dependendo do objetivo da aplicação, superfícies de volatilidade podem ser interpoladas, suavizadas ou parametrizadas. A parametrização tem recebido especial interesse pois pode, ao mesmo tempo que garante uma superfície livre de arbitragem estática se devidamente construída, ajustar-se muito bem aos dados observados e gerar distribuições neutras ao risco implícitas factíveis.</p>
<p>Para gerar uma superfície parametrizada, primeiramente é necessário um modelo teórico com propriedades desejáveis e que se ajuste aos dados de mercado quando calibrado. Escolhido este modelo paramétrico, passa-se a calibração do mesmo onde exsitem diversas opções de escolha entre otimizadores. Ao final do processo teremos um modelo de superfície devidamente parametrizado com valores que melhor se ajustam segundo alguma função perda escolhida.</p>
<p>Com a superfície de volatilidade calibrada, as aplicações possíveis incluem a precificação de derivativos, gerenciamento de risco, simulações de Monte Carlo, análises de stress, entre outras.</p>
</div>
<div id="referencias" class="section level2 unnumbered">
<h2>Referências</h2>
<div id="refs" class="references">
<div id="ref-Dupire1994">
<p>Dupire, Bruno. 1994. “Pricing with a Smile.” <em>Risk</em> 7 (1): 18–20.</p>
</div>
<div id="ref-Gatheral2004">
<p>Gatheral, Jim. 2004. “A Parsimonious Arbitrage-Free Implied Volatility Parameterization with Application to the Valuation of Volatility Derivatives.” <em>Presentation at Global Derivatives &amp; Risk Management, Madrid</em>.</p>
</div>
<div id="ref-Heston1993">
<p>Heston, Steven L. 1993. “A Closed-Form Solution for Options with Stochastic Volatility with Applications to Bond and Currency Options.” <em>The Review of Financial Studies</em> 6 (2). Oxford University Press: 327–43.</p>
</div>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Veja mais detalhes no artigo anterior: <a href="/2019/01/25/smile-de-volatilidade-parte-2">Smile de volatilidade - parte 2</a><a href="#fnref1">↩</a></p></li>
<li id="fn2"><p>Definição retirada de <a href="https://pt.wikipedia.org/wiki/Spline" class="uri">https://pt.wikipedia.org/wiki/Spline</a><a href="#fnref2">↩</a></p></li>
<li id="fn3"><p>A variância total é definida pelo produto entre a variância implícita e o tempo para expiração, <span class="math inline">\(w=\sigma^2_{imp}\cdot\tau\)</span>.<a href="#fnref3">↩</a></p></li>
</ol>
</div>
