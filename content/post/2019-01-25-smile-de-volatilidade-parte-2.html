---
title: Smile de volatilidade - parte 2
author: Rafael Bressan
date: '2019-01-25'
categories:
  - Derivativos
tags:
  - opções
  - Quant
  - smile
  - Volatilidade
slug: smile-de-volatilidade-parte-2
aliases: /post/smile-parte-2
bibliography: library.bib
link-citations: yes
---



<p>Daremos continuidade ao <a href="/2019/01/18/smile-de-volatilidade">artigo anterior</a> sobre o smile de volatilidade. Falaremos sobre a estrutura a termo da volatilidade implícita, agregando uma segunda dimensão ao smile e transformando-o na famigerada <strong>superfície de volatilidade implícita</strong>. Também será definida o que é a arbitragem estática e seus tipos e como a limitação da presença de arbitragem estática impõe restrições na superfície de volatilidade. Por fim, será demonstrado como, a partir de um smile de volatilidade é possível derivar a distribuição implícita neutra ao risco do subjacente para data de expiração das opções.</p>
<div id="estrutura-a-termo" class="section level2">
<h2>Estrutura a termo</h2>
<p>O mercado precifica a volatilidade implícita de forma que esta dependa também do tempo até expiração, bem como do preço de exercício.</p>
<p>A volatilidade implícita tende a ser uma função crescente da maturidade quando as volatilidades de curto prazo são historicamente baixas e função decrescente da maturidade quando as volatilidades de curto prazo são historicamente altas. Isso porque existe uma expectativa de reversão a uma média de longo prazo embutida na volatilidade. Esta característica é explorada explicitamente por alguns modelos de volatilidade, como em <span class="citation">Heston (<a href="#ref-Heston1993">1993</a>)</span>.</p>
<p>As superfícies de volatilidade combinam smiles com a estrutura a termo de volatilidade para tabular valores apropriados para precificar uma opção com qualquer preço de exercício e prazo de expiração.</p>
<p>Da mesma forma como a curva de juros em um dado momento é uma descrição concisa dos preços dos títulos negociados naquele mercado, assim, para um ativo subjacente em particular em determinado momento, a superfície de volatilidade implícita fornece uma descrição resumida de seu mercado de opções. Considerando que os rendimentos dos títulos são diferenciados pelo seu tempo até o vencimento, as opções são diferenciadas por seu tempo até a expiração e o <em>strike</em>, logo requerem uma superfície ao invés de uma curva.</p>
<p>A figura <a href="#fig:superficie">1</a> demonstra uma superfície de volatilidade implícita do <code>SPX</code> em 15/09/2005, conforme apresentado em <span class="citation">Gatheral (<a href="#ref-Gatheral2011">2011</a>)</span>.</p>
<div class="figure"><span id="fig:superficie"></span>
<img src="/images/spx_vol_surface.png" alt="Superfície de volatilidade implícita."  />
<p class="caption">
Figura 1: Superfície de volatilidade implícita.
</p>
</div>
</div>
<div id="arbitragem-estatica" class="section level2">
<h2>Arbitragem estática</h2>
<p>Antes de definir o que é arbitragem estática que pode estar presente em uma superfície de volatilidade (ou na superfície de preço de opções), vamos partir para a intuição por trás desta definição.</p>
<p>O princípio de ausência de arbitragem é dominante na teoria financeira. Este princípio nos informa que não deve existir lucro sem que se incorra em algum tipo de risco, o lucro sempre é a remuneração do investidor que aceitou carregar alguma forma de risco durante o investimento. Portanto, não devem existir perfis de lucro acima da taxa livre de risco (<em>payoffs</em> positivos) com probabilidade de 100%.</p>
<p>Primeiro consideramos uma trava de alta com opções do tipo call. Excluindo-se os custos de transação, esta operação sempre oferece um retorno positivo ou zero, conforme a figura <a href="#fig:trava-alta">2</a>. Por mais que esta estratégia esteja montada fora do dinheiro, sempre existe uma possibilidade de ela ter lucro, <span class="math inline">\(S_T&gt;K\)</span> e portanto seu preço deve ser sempre maior que zero.</p>
<div class="figure"><span id="fig:trava-alta"></span>
<img src="/images/trava_alta.png" alt="Perfil de lucro de uma trava de alta."  />
<p class="caption">
Figura 2: Perfil de lucro de uma trava de alta.
</p>
</div>
<p>É claro que quanto mais ITM estejam as opções, maior seu preço e quanto mais fora do dinheiro menor será seu valor até o limite inferior zero. Se levarmos a diferença entre os <em>strikes</em>, <span class="math inline">\(dK\)</span> a zero temos que:</p>
<p><span class="math display">\[\frac{\partial C}{\partial K}\leq 0\]</span></p>
<p>Este é o limite de arbitragem para travas de alta ou, mais conhecido pelo termo em inglês <em>call spread no-arbitrage</em> e impõe que os preços das calls devem ser uma função descrescente no <em>strike</em>. De forma equivalente e através da <a href="/2018/10/04/modelo-black-scholes-merton">paridade compra-venda</a> este limite de arbitragem para as puts é:</p>
<p><span class="math display">\[\frac{\partial P}{\partial K}\geq 0\]</span></p>
<div id="arbitragem-de-borboleta" class="section level3">
<h3>Arbitragem de borboleta</h3>
<p>Também deve ser imposta uma restrição na segunda derivada do preço das opções em relação ao <em>strike</em>, e esta é conhecida como limite de arbitragem para borboletas. Vejamos porquê.</p>
<p>Considere uma estratégia do tipo borboleta, onde se compra uma quantia de calls no <em>strike</em> <span class="math inline">\(K-dK\)</span>, vende-se duas vezes esta quantia em <span class="math inline">\(K\)</span> e compra-se novamente um lote em <span class="math inline">\(K+dK\)</span>, o perfil de lucro desta operação no vencimento está representado na figura <a href="#fig:borboleta">3</a>.</p>
<div class="figure"><span id="fig:borboleta"></span>
<img src="/images/borboleta.png" alt="Borboleta realizada com calls."  />
<p class="caption">
Figura 3: Borboleta realizada com calls.
</p>
</div>
<p>Seguindo a mesma linha de raciocínio anterior, como o <em>payoff</em> da borboleta é sempre não negativo também deve ser o seu valor para qualquer período anterior a expiração. Se denotarmos <span class="math inline">\(\pi_B\)</span> o valor da borboleta, então <span class="math inline">\(\pi_B\geq0\)</span>.</p>
<p>Agora imagine que escalamos a estratégia de forma que um lote de compras (na venda são dois lotes) seja de tamanho <span class="math inline">\(1/dK^2\)</span>, o valor para a montagem desta operação deve ser, portanto:</p>
<p><span class="math display">\[
\pi_B=\frac{C(K-dK)-2C(K)+C(K+dK)}{dK^2}
\]</span></p>
<p>E se levarmos ao limite em que <span class="math inline">\(dK\rightarrow 0\)</span>, a equação acima torna-se justamente a segunda derivada do preço da call no <em>strike</em> <span class="math inline">\(K\)</span>.</p>
<p><span class="math display">\[
\begin{aligned}
\frac{\partial^2 C(K)}{\partial K^2}=&amp; \pi_B\\
\geq &amp; 0
\end{aligned}
\]</span></p>
<p>Ou seja, os preços das calls são uma função <strong>convexa</strong> nos <em>strikes</em>. O mesmo raciocínio pode ser feito para uma borboleta com puts e o resultado será equivalente, o preço das puts também deve ser uma função convexa nos <em>strikes</em>.</p>
</div>
<div id="arbitragem-de-calendario" class="section level3">
<h3>Arbitragem de calendário</h3>
<p>Passamos agora a analisar os limites de arbitragem na estrutura a termo da superfície de volatilidade. A arbitragem de calendário normalmente é expressa em termos de monotonicidade dos preços em relação ao período para expiração. Ou seja, quanto maior o prazo de maturidade de uma opção para um mesmo preço de exercício, maior deve ser seu valor.</p>
<p>É fácil de entender este limite com base nas probabilidades de exercício. Como sabemos, em um <a href="/2018/09/25/processos-estocasticos-em-financas">processo estocástico</a> do tipo MBG a variância do processo cresce conforme a <strong>raiz do tempo</strong>, <span class="math inline">\(\sqrt{\tau}\)</span>. Quanto maior a variância do ativo subjacente, maior a probabilidade deste alcançar um determinado preço, mais elevado ou não. Assim, seja uma call ou put OTM quanto mais distante estiver seu prazo de maturidade, maior a probabilidade de exercício e portanto, maior seu valor.</p>
<p>Dado que a relação de <strong>volatilidade total</strong> implícita e preço de uma opção também é direta e positiva, conforme demonstrado na <a href="/2019/01/18/smile-de-volatilidade">parte 1</a> deste artigo, segue que a volatilidade total deve ser não decrescente no tempo para expiração.</p>
<p>Esta relação pode ser expressa através da seguinte equação para uma call precificada através de B&amp;S:</p>
<p><span class="math display">\[
\frac{\partial C_{BS}(k, \theta(\tau))}{\partial \tau}=\partial_\theta C_{BS}\cdot\partial_\tau \theta \geq 0
\]</span></p>
<p>onde <span class="math inline">\(\partial_\theta C_{BS}\)</span> é a derivada parcial do preço da call em relação a volatilidade total implícita, que já demonstramos ser positiva e <span class="math inline">\(\partial_\tau \theta\)</span> é a derivada parcial da volatilidade total implícita em relação ao tempo para maturidade que, portanto, deve ser maior ou igual a zero para obedecer a restrição imposta ao preço da call.</p>
</div>
</div>
<div id="limites-de-inclinacao" class="section level2">
<h2>Limites de inclinação</h2>
<p>Se mantivermos a volatilidade implícita constante para todos os <em>strikes</em>, os preços das calls no modelo B&amp;S devem ser decrescentes. Por outro lado, para um <em>strike</em> fixo, o preço de uma call se eleva à medida que a volatilidade implícita aumenta. Suponha por um momento que a volatilidade implícita varia com o <em>strike</em> como é o caso nos smiles. À medida que o <em>strike</em> aumenta, se a volatilidade implícita aumentar muito rapidamente, seu efeito sobre o preço da call pode mais que compensar o declínio no preço devido a elevação do preço de exercício e, assim, levar a um aumento líquido no preço da opção. Isso violaria o requisito de que <span class="math inline">\(\partial C /\partial K \leq 0\)</span> e, portanto, leva a um limite superior na taxa em que a volatilidade implícita pode aumentar com o strike.</p>
<p>Novamente, o mesmo raciocínio pode ser imposto para o lado das puts. A volatilidade implícita não pode se elevar tão rapidamente quando os <em>strikes</em> se reduzem de forma que uma put de <em>strike</em> menor tenha valor mais elevado que outra que esteja mais próxima do dinheiro.</p>
<p>Finalmente, um sumário dos limites impostos a uma superfície de preços de opções (calls no caso apresentado), que implicam em limites para a superfície de volatilidade é apresentado abaixo<a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a>:</p>
<ol style="list-style-type: decimal">
<li><span class="math inline">\(\partial_\tau C \geq 0\)</span></li>
<li><span class="math inline">\(\lim\limits_{K\rightarrow\infty}C(K, \tau)=0\)</span></li>
<li><span class="math inline">\(\lim\limits_{K\rightarrow-\infty}C(K, \tau)+K=a, \quad a \in \mathbb R\)</span></li>
<li><span class="math inline">\(C(K, \tau)\)</span> é convexa em <span class="math inline">\(K\)</span></li>
<li><span class="math inline">\(C(K, \tau)\)</span> é não-negativa</li>
</ol>
</div>
<div id="distribuicao-implicita" class="section level2">
<h2>Distribuição implícita</h2>
<p>O modelo B&amp;S é baseado na suposição que o ativo subjacente segue uma distribuição log-normal em seus preços. Caso esta suposição fosse de fato realizada no mercado, o smile de volatilidade seria uma reta completamente horizontal, não haveria variação na volatilidade implícita conforme o preço de exercício. Entretanto, esta não é a realidade dos smiles e podemos fazer a pergunta inversa portanto, qual a distribuição neutra ao risco que está implícita no smile de volatilidade?</p>
<p>Certamente não é uma log-normal. Na verdade, a densidade da distribuição que está implícita em um smile nada mais é que a convexidade deste smile, ou seja, sua segunda derivada em relação ao <em>strike</em>. Esta distribuição implícita também é por vezes chamada de RND <em>(risk neutral density)</em> e é muito útil para fazer a precificação de outras opções que não são observadas no smile ou extrair probabilidades de ocorrência de eventos precificadas pelo mercado.</p>
<p>Pode-se obter este resultado a partir da definição do valor de uma call e é conhecido como a fórmula de Breeden-Litzenberger<a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a>. O valor de uma call é o valor esperado do <em>payoff</em> terminal desta call ponderado pela densidade neutra ao risco do subjacente. Ou seja:</p>
<p><span class="math display">\[
C(S, t)=e^{-r\tau}\int\limits_{0}^\infty p(S,t,S_T,T)\max\{S_T-K, 0\}dS_T
\]</span></p>
<p>onde <span class="math inline">\(p(\cdot)\)</span> é a densidade neutra ao risco e estamos supondo uma taxa de juros livre de risco constante durante o período de vida da opção. Como o <em>payoff</em> da call é não linear, sendo zero para qualquer valor de <span class="math inline">\(S_T \leq K\)</span> e igual a <span class="math inline">\(S_T-K\)</span> quando <span class="math inline">\(S_T &gt; K\)</span>, podemos escrever esta equação como:</p>
<p><span class="math display">\[
C(S, t)=e^{-r\tau}\int\limits_{K}^\infty p(S,t,S_T,T)(S_T-K)dS_T
\]</span></p>
<p>que pode ser rearranjada, com alguma simplificação na notação, da seguinte forma.</p>
<p><span class="math display">\[
\begin{aligned}
\frac{\partial C}{\partial K}=&amp; -e^{-r\tau}\int\limits_{K}^\infty p(S_T)dS_T\\
e^{r\tau}\frac{\partial C}{\partial K}=&amp; \int\limits_{-\infty}^K p(S_T)dS_T\\
e^{r\tau}\frac{\partial^2 C}{\partial K^2}=&amp; \ p(K)\\
\frac{\partial^2 C_B}{\partial K^2}=&amp; \ p(K)\\
\end{aligned}
\]</span></p>
<p>Onde usou-se a notação <span class="math inline">\(C_B\)</span> para denotar a formulação de Black para o preço de uma call. Ou seja, a segunda derivada em relação ao strike do preço não descontado de uma call é a distribuição neutra ao risco do ativo subjacente, e é válida para todos preços de exercício.</p>
<p>Portanto, se desejarmos saber qual a distribuição de probabilidades de preços do ativo subjacente em uma data futura que possua vencimento de opções, basta encontrarmos a convexidade do smile dos preços <em>forward</em> daquele vencimento<a href="#fn3" class="footnoteRef" id="fnref3"><sup>3</sup></a>.</p>
</div>
<div id="conclusao" class="section level2">
<h2>Conclusão</h2>
<p>Este foi um artigo denso, porém com vários conceitos importantes para a compreensão do comportamento da superfície de volatilidade. A estrutura a termo também é existente na volatilidade implícita e está limitada pela ausência de arbitragem do tipo calendário. O smile de volatilidade, que é uma fatia da superfície com prazo de expiração constante, possui suas próprias limitações de forma, com a ausência de arbitragem do tipo borboleta e limitações quanto a inclinação.</p>
<p>Por fim, foi demonstrado como a convexidade do smile de preços fornece a distribuição implícita para os preços do ativo subjacente para a data de expiração das opções.</p>
</div>
<div id="referencias" class="section level2 unnumbered">
<h2>Referências</h2>
<div id="refs" class="references">
<div id="ref-Aurell2014">
<p>Aurell, Alexander. 2014. “The Svi Implied Volatility Model and Its Calibration.” Master’s thesis, Kungliga Tekniska Högskolan.</p>
</div>
<div id="ref-Breeden1978">
<p>Breeden, Douglas T, and Robert H Litzenberger. 1978. “Prices of State-Contingent Claims Implicit in Option Prices.” <em>Journal of Business</em>. JSTOR, 621–51.</p>
</div>
<div id="ref-Gatheral2011">
<p>Gatheral, Jim. 2011. <em>The Volatility Surface: A Practitioner’s Guide</em>. Vol. 357. John Wiley &amp; Sons.</p>
</div>
<div id="ref-Heston1993">
<p>Heston, Steven L. 1993. “A Closed-Form Solution for Options with Stochastic Volatility with Applications to Bond and Currency Options.” <em>The Review of Financial Studies</em> 6 (2). Oxford University Press: 327–43.</p>
</div>
</div>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Retirado de <span class="citation">Aurell (<a href="#ref-Aurell2014">2014</a>)</span>, p. 25.<a href="#fnref1">↩</a></p></li>
<li id="fn2"><p>Autores da formulação em seu artigo, <span class="citation">Breeden and Litzenberger (<a href="#ref-Breeden1978">1978</a>)</span><a href="#fnref2">↩</a></p></li>
<li id="fn3"><p>Simples em teoria, muito mais complicado na prática, com diversos problemas para a extrapolação do smile para <em>strikes</em> extremos.<a href="#fnref3">↩</a></p></li>
</ol>
</div>
