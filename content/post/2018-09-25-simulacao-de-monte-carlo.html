---
title: Simulação de Monte Carlo
author: Rafael Bressan
date: "2018-09-25"
categories:
  - Derivativos
  - Programação
tags:
  - opções
  - Quant
  - R
  - Monte Carlo
slug: monte-carlo
aliases: /post/monte-carlo
bibliography: library.bib
link-citations: true
---



<p>Em artigo anterior, sobre processos estocásticos, fizemos uso de uma poderosa ferramenta computacional, frequentemente utilizada em finanças para fazer simulações. Naquele artigo simulamos cinco realizações de caminhos de um processo estocástico, cada um com 500 passos a frente. Esta técnica é conhecida como Simulação de Monte Carlo - SMC e será abordada no presente artigo.</p>
<p>Neste artigo também iremos introduzir, no corpo do texto, os códigos em linguagem <a href="https://cran.r-project.org/">R</a> utilizados para fazer as simulações, aumentando a didática de nossos artigos. A linguagem R é uma das preferidas para a modelagem estatística, é uma das linguagens de ciência de dados que vem ganhando muitos adeptos e, por conseguinte, é amplamente utilizada pelo mercado financeiro. E claro, é uma das preferidas aqui do CF também.</p>
<p>Nosso problema será simular a posição de um portfólio composto por uma posição comprada em uma ação <code>PETR4</code> e uma <em>put</em> <code>PETRV17</code>. A opção de venda (<em>put</em>) tem preço de exercício em R$ 16,92, data de expiração em 15/10/2018 e é do tipo europeia. Ao final do pregão do dia 21/09/2018 a ação <code>PETR4</code> fechou cotada a R$ 20,14 e a <em>put</em> em R$ 0,12. A partir desta data até o dia da expiração da opção haverão 16 dias de pregão, que será nosso horizonte de simulação.</p>
<p>Para melhoria da didática do texto e também para simplificação do problema, manteremos algumas variáveis necessárias para a precificação de opções constantes ao longo do período de análise, são elas:</p>
<ul>
<li><p>Volatilidade: Será calculada a volatilidade implícita da opção da data de compra do portfólio, 21/09/2018, e será mantida constante a partir daí para fins de precificação na SMC;</p></li>
<li><p>Taxa de juros: constante no valor de 6,5 %a.a. tem termos contínuos;</p></li>
<li><p>Taxa de dividendos: suposto igual a zero.</p></li>
</ul>
<div id="simulacao-de-monte-carlo" class="section level2">
<h2>Simulação de Monte Carlo</h2>
<p>Para realizar uma SMC de um ativo financeiro deve-se primeiramente estabelecer uma distribuição de probabilidades que os retornos deste ativo deve seguir. Em nosso exemplo, utilizaremos a distrbuição normal para os retornos logarítimicos da ação <code>PETR4</code>, em linha com o clássico modelo <em>Black &amp; Scholes</em>, certamente existem diversas variantes que se ajustam melhor a realidade dos mercados, entretanto este é o modelo mais conhecido e base de todos os demais.</p>
<p>Uma vez escolhida a distribuição dos (log) retornos, tem-se de escolher valores para a média e variância desta distribuição. A média dos retornos iremos tirar do histórico da ação, o retorno médio diário dos último ano. A variância da distribuição será encontrada a partir da volatilidade implícita da opção na data de compra do portfólio. A função utilizada para encontrar esta volatilidade retorna um valor em termos anuais, portanto, conforme visto no artigo sobre <a href="/post/processos-estocasticos">processos estocásticos</a> devemos reescalar uma volatilidade anual para diária, e isto é obtido fazendo a divisão por <span class="math inline">\(\sqrt{252}\)</span>, onde temos 252 dias úteis em 1 ano.</p>
<p>Desta forma é possível fazer a simulação dos log-retornos da ação para cada um dos dias a frente, até a data de exercício da opção, 15/10/2018. Estes retornos são acumulados e o preço <strong>simulado</strong> da ação <code>PETR4</code> em uma data intermediária é o preço de compra vezes o retorno acumulado até então.</p>
<p>Faremos 1.000 simulações destas, gerando caminhos possíveis de preços para a ação. É necessário fazer muitas simulações para termos uma boa ideia da distribuição dos preços na data final, não é raro serem feitas mais de mil simulações, as vezes até dez mil podem ser necessárias.</p>
<p>Uma vez gerados todos os caminhos simulados do preço do ativo objeto, podemos então precificar a <em>put</em> com base nestes preços simulados e as outras variáveis necessárias para se precificar uma opção europeia. Assim teremos também todos os caminhos de preço para a opção até sua data de exercício.</p>
<p>O valor de nosso portfólio, em qualquer ponto do intervalo de tempo em análise, será a soma do preço da ação com o preço da opção e será possível verificar o efeito de proteção contra quedas do preço do ativo objeto a compra da <em>put</em> tem no portfólio.</p>
<p>Cabe ressaltar aqui que o preço da opção <strong>não</strong> é simulado, não diretamente. Como a opção é um instrumento derivativo o seu preço “deriva” do preço do ativo objeto, este sim que é simulado. Uma vez que tenhamos o preço da ação, dadas nossas premissas de precificação, podemos calcular o prêmio da opção com base no modelo <em>Black &amp; Scholes</em>.</p>
</div>
<div id="implementacao-em-r" class="section level2">
<h2>Implementação em R</h2>
<p>Conforme comentado, utilizamos aqui no CF a linguagem de programação <a href="https://cran.r-project.org/">R</a> para realizar nossas atividades que envolvam métodos quantitativos em finanças. Abaixo irei apresentar o código utilizado, trecho a trecho e o resultado obtido ao final.</p>
<p>Primeiramente, no R, devemos carregar em nossa sessão de trabalho os pacotes que serão utilizados ao longo do código. Os pacotes funcionam como extensões ao R base, nestes pacotes encontramos diversas funções já programadas por outras pessoas que facilitam (e muito!) a nossa codificação.</p>
<pre class="r"><code>library(tidyverse)
library(ggthemes)
library(tidyquant)
library(RQuantLib)</code></pre>
<p>O pacote <code>RQuantLib</code>, por exemplo, possui já implementado dentro dele funções para fazer a precificação de opções europeias, sem que se tenha que implementar o modelo manualmente. Como a intenção deste artigo não é explicar o modelo <em>Black &amp; Scholes</em>, vamos abstrair esta parte e simplesmente chamar uma função que nos retorna o valor da opção dadas as variáveis de entrada.</p>
<p>Em seguida iremos definir algumas de nossas variáveis, como o <em>ticker</em> da ação para buscar seus dados históricos através da função <code>tq_get()</code> do pacote <code>tidyquant</code> e calcular os retornos logarítimicos e tirar sua média, o preço e data de exercício da opção e também iremos relacionar os dias de negócio entre a data de compra e vencimento.</p>
<pre class="r"><code>acao &lt;- &quot;PETR4.SA&quot;
p_exer &lt;- 16.92
d_exer &lt;- as.Date(&quot;2018-10-15&quot;)
d_atual &lt;- as.Date(&quot;2018-09-21&quot;)
dias &lt;- seq(d_atual, d_exer, by = 1)
dias &lt;- dias[isBusinessDay(&quot;Brazil&quot;, dias)]
nsims &lt;- 1000
ndias &lt;- length(dias) - 1
sim_nomes &lt;- paste0(&quot;sim&quot;, 1:nsims)

# Carregar os precos historicos da acao
p_hist &lt;- tq_get(acao, from = d_atual - years(1), to = d_atual + days(1)) %&gt;% 
  filter(volume != 0.0)
ret_hist &lt;- p_hist %&gt;% 
  tq_mutate(select = adjusted,
            mutate_fun = periodReturn,
            period = &quot;daily&quot;,
            type = &quot;log&quot;,
            leading = FALSE,
            col_rename = &quot;log_ret&quot;) %&gt;% 
  na.omit()
rf &lt;- log(1 + 0.065)
div &lt;- 0
S0 &lt;- last(ret_hist$adjusted)
P0 &lt;- 0.12
mi &lt;- 252 * mean(ret_hist$log_ret) # retorno medio em termos anuais
sigma &lt;- EuropeanOptionImpliedVolatility(&quot;put&quot;, P0, S0, p_exer, div, rf, 
                                         (ndias + 1) / 252, 0.30)</code></pre>
<p>Com o código acima obtemos basicamente todos os dados com os quais poderemos implementar a simulação de Monte Carlo. Entretanto, para realizar as simulações, necessitamos especificar mais algumas funções customizadas para nossas necessidades.</p>
<p>Primeiro iremos especificar uma função que retorna uma única simulação de log-retornos acumulados em uma coluna de dados, esta função é chamada de <code>mc_sim_fun</code>. A segunda função necessária é a função de precificação da opção europeia. Por padrão, a função do pacote <code>RQuantLib</code> <code>EuropeanOption()</code> retorna uma lista com o valor da opção, mas também todas as suas gregas. Também de forma um tanto quanto estranha, esta função retorna o valor zero na data de exercício, mesmo que a opção esteja <a href="http://clubedefinancas.com.br/materias/introducao-ao-mercado-de-opcoes/">In-The-Money</a>, portanto é necessário modificar este comportamento.</p>
<pre class="r"><code># Funcao para realizar uma simulacao
mc_sim_fun &lt;- function(valor_i, N, media, volat){
  med_d &lt;- media / 252
  volat_d &lt;- volat / sqrt(252)
  ans &lt;- tibble(c(valor_i, rnorm(N, med_d - (volat_d^2 / 2), volat_d))) %&gt;% 
    `colnames&lt;-`(&quot;log_ret&quot;) %&gt;%
    mutate(ret_ac = cumsum(log_ret)) %&gt;% 
    select(ret_ac)
  
  return(ans)
}

# Funcao para precificar uma opcao europeia
eur_option &lt;- function(type, underlying, strike, dividendYield, riskFreeRate, 
                       maturity, volatility) {
  if (maturity == 0.0) {
    ans &lt;- switch(type,
                  put = max(strike - underlying, 0),
                  call = max(underlying - strike, 0))
    return(ans)
  }
  
  ans &lt;- EuropeanOption(type = type,
                        underlying = underlying,
                        strike = strike,
                        dividendYield = dividendYield,
                        riskFreeRate = riskFreeRate,
                        maturity = maturity,
                        volatility = volatility)$value
  return(ans)
}</code></pre>
<p>Uma vez com os dados obtidos e as funções auxiliares programadas, podemos passar a SMC propriamente dita. Aqui vamos estabelecer o número de simulações (1.000), calcular um <em>data frame</em> com os log-retornos acumulados e então calcular o preço da ação para cada dia e simulação realizados. O preço da ação na data <span class="math inline">\(t\)</span> será <span class="math inline">\(S_t=S_0 e^{r_t}\)</span>, onde <span class="math inline">\(r_t\)</span> é o log-retorno acumulado até a data <span class="math inline">\(t\)</span>.</p>
<p>Após termos todos os preços do ativo objeto, passamos a computar qual seria o preço da opção, <span class="math inline">\(P_t\)</span>, naquelas condições. O valor do portfólio é dado pela soma destes dois preços (lembre-se, nosso portfólio é composto por <strong>uma</strong> ação e uma opção de venda).</p>
<pre class="r"><code># Simulacao de Monte Carlo
# Valores Iniciais
inic &lt;- rep(0, nsims) 
set.seed(12345)
ret_ac_mc &lt;- map_dfc(inic,
                     mc_sim_fun,
                     N = ndias,
                     media = mi,
                     volat = sigma)

precos_acao &lt;- (S0 * exp(ret_ac_mc)) %&gt;% 
  set_names(sim_nomes) %&gt;% 
  mutate(anos_exp = (ndias:0) / 252) %&gt;% 
  gather(key = sims, value = St, -anos_exp)

# Evolucao do Portfolio
port_mc &lt;- precos_acao %&gt;% 
  mutate(Pt = map2_dbl(St, anos_exp, 
                       ~eur_option(type = &quot;put&quot;,
                                   underlying = .x,
                                   strike = p_exer,
                                   dividendYield = div,
                                   riskFreeRate = rf,
                                   maturity = .y,
                                   volatility = sigma)),
         port_valor = Pt + St,
         data = rep(dias, nsims))
head(port_mc)</code></pre>
<pre><code>##     anos_exp sims       St         Pt port_valor       data
## 1 0.05952381 sim1 19.40838 0.10764938   19.51603 2018-09-21
## 2 0.05555556 sim1 19.75814 0.06776911   19.82591 2018-09-24
## 3 0.05158730 sim1 20.18780 0.03648809   20.22429 2018-09-25
## 4 0.04761905 sim1 20.13504 0.03167337   20.16671 2018-09-26
## 5 0.04365079 sim1 19.87972 0.03455686   19.91428 2018-09-27
## 6 0.03968254 sim1 20.25012 0.01649154   20.26661 2018-09-28</code></pre>
<p>O <em>data frame</em> <code>port_mc</code> contém todas as informações da SMC de nosso portfólio. Contém as datas desde o dia da compra até a data de vencimento da opção e contém <strong>todos</strong> os caminhos de <span class="math inline">\(S_t\)</span>, <span class="math inline">\(P_t\)</span> e do portfólio. Vamos plotar os resultados obtidos para a evolução apenas da ação, primeiramente.</p>
<pre class="r"><code>brk &lt;- round(sort(c(p_exer, seq(min(port_mc$St),
                                max(port_mc$St),
                                length.out = 5))),
             digits = 2)
ggplot(port_mc, aes(x = data, y = St)) + 
  geom_line(aes(color = sims)) +
  geom_hline(yintercept = p_exer, color = &quot;red&quot;) +
  guides(color = FALSE) +
  labs(title = &quot;Simulações do Valor da Ação&quot;,
       x = &quot;data&quot;,
       y = &quot;Valor (R$)&quot;) +
  scale_y_continuous(breaks = brk) +
  scale_x_date(date_breaks = &quot;2 days&quot;, date_labels = &quot;%d&quot;) +
  scale_color_viridis_d() +
  theme_economist_white()</code></pre>
<p><img src="/post/2018-09-25-simulacao-de-monte-carlo_files/figure-html/gr_acao-1.png" width="672" /></p>
<p>Podemos verificar pela figura acima que a ação, pela nossa SMC, deve fechar na maioria dos caminhos simulados acima do preço de exercício da <em>put</em> (linha vermelha). Entretanto existe uma menor probabilidade de, até a data de vencimento, o preço da ação cair abaixo do <em>strike</em> desta opção.</p>
<p>Podemos inferir esta probabilidade através do número de caminhos que terminaram em preço da ação abaixo do valor de referência. O custo de proteção contra este risco é o prêmio por nós ao comprarmos a <em>put</em>. O código para esta inferência está abaixo.</p>
<pre class="r"><code>p_baixo &lt;- port_mc %&gt;% 
  filter(data == d_exer) %&gt;% 
  summarise(num_baixo = sum(St &lt; p_exer)) %&gt;% 
  as.double()
prob &lt;- p_baixo / nsims</code></pre>
<p>Este cálculo nos mostra que em 108 caminhos simulados do preço de <code>PETR4</code>, este terminou abaixo do preço de exercío da opção <code>PETRV17</code>, ou seja, uma probabilidade de 10.8%.</p>
<p>Para nos precavermos desta possível queda de preço e garantir um valor mínimo de nosso portfólio até a data de 15/10/2018, podemos comprar uma opção de venda, com preço de exercício no valor que desejamos e então o portfólio passa a ser composto pela ação e também pela opção. Caso na data de vencimento o preço da ação seja menor que o preço de exercício da <em>put</em>, esta opção estará ITM e pode ser exercida pelo valor da diferença entre os preços, ou seja, nos garantindo que nosso portfólio estará avaliado em R$ 16,92.</p>
<p>Esta dinâmica pode ser verificada pela figura abaixo, que agora apresenta o valor do portfólio completo, ação mais opção. Verificamos que, de fato, no dia 15/10/2018 nosso investimento não estará em situação pior que o preço garantido pela compra da <em>put</em>.</p>
<pre class="r"><code>brk &lt;- round(sort(c(p_exer, seq(min(port_mc$port_valor),
                                max(port_mc$port_valor),
                                length.out = 5)[-1])),
             digits = 2)
ggplot(port_mc, aes(x = data, y = port_valor)) + 
  geom_line(aes(color = sims)) +
  geom_hline(yintercept = p_exer, color = &quot;red&quot;) +
  guides(color = FALSE) +
  labs(title = &quot;Simulações do Valor do Portfolio&quot;,
       x = &quot;data&quot;,
       y = &quot;Valor (R$)&quot;) +
  scale_y_continuous(breaks = brk) +
  scale_x_date(date_breaks = &quot;2 days&quot;, date_labels = &quot;%d&quot;) +
  scale_color_viridis_d() +
  theme_economist_white()</code></pre>
<p><img src="/post/2018-09-25-simulacao-de-monte-carlo_files/figure-html/gr_port-1.png" width="672" /></p>
<p>Ou seja, ao custo de 0.62% do preço da ação, compramos uma proteção contra uma queda de preços com probabilidade de 10.8%.</p>
<p>Esta é apenas uma (simples) aplicação das inúmeras possíveis que a Simulação de Monte Carlo possui no mundo das finanças. A SMC é uma poderosa ferramenta para avaliação e controle de risco de grandes portfólios, com centenas ou milhares de ativos, onde nem sempre consegue-se aferir medidas de retorno esperado ou de risco de mercado de forma analítica.</p>
</div>
