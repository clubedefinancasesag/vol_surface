# Superfície de Volatilidade com R

**Projeto:** Interpolação de uma superfície de volatilidade implícita

**Data final:** 01/03/2019

**Obejetivo:** A partir de dados de volatilidade implícita de opções sobre futuros da taxa de câmbio, derivar a superfície de volatilidade nas dimensões do preço de exercício e da maturidade. Para gerar esta superfície, deve-se utilizar algum método de interpolação, a mais comum é a _cubic spline_. As séries vêm com deltas constantes, no lugar dos preços de exercício, de forma que antes de fazer a interpolação é necessário transformar o valor do delta no preço de exercício equivalente.

**Roteiro**: Estudar os fundamentos de processos estocásticos, o movimento Browniano (Geométrico) e a fórmula de Black \& Scholes. Entender o que são e para que servem as gregas que surgem do modelo BS. Precificação através de volatilidade implícita e estrutura a termo desta volatilidade. A superfície de volatilidade, sua construção e métodos para interpolação. O livro texto básico será o _"Options, Futures and Other Derivatives"_ do John Hull[^1] com textos retirados de outras fontes. Vídeo-aulas do MIT _Open Courseware_ podem ser utilizadas como suplemento. Cada capítulo tem uma data de apresentação onde cada membro, **individualmente**, apresentará seus estudos (não são necessários slides) para os outros membros e haverá uma discussão. Um ou dois membros estão designados para escrever um artigo sobre o capítulo estudado a ser postado no site do CF. A data do artigo é a próxima segunda-feira após o encontro.

[^1]: [HULL](https://drive.google.com/drive/folders/0B2xLJTfoxbzRRVNrMFNMbDhNRW8) Options, Futures and Other Derivatives. 9th Edition.