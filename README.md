[![Netlify Status](https://api.netlify.com/api/v1/badges/59c8090b-d0ff-4242-b99b-5625d1dd473b/deploy-status)](https://app.netlify.com/sites/volatilidade/deploys)

# Volatilidade Implícita
 
Este é um projeto de estudo desenvolvido pelo núcleo de Derivativos&Riscos do [Clube de Finanças Esag](clubedefinancas.com.br) para aprimorarmos nossos conhecimentos sobre opções, seus modelos de precificação e superfícies de volatilidade implícita.

O cronograma proposto com os assuntos a serem estudados e os artigos resultantes deste projeto podem ser acessados em [Superfície de Volatilidade com R](volatilidade.netlify.com).

Novos conteúdos podem ser inseridos a partir do repositório `vol_surface` do CF no GitLab. O conteúdo será automaticamente carregado para o site ao se fazer um commit/push no ramo master do repositório.
