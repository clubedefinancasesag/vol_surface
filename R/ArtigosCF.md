# Artigos publicados no site do CF

[Fama e French](http://clubedefinancas.com.br/materias/conheca-o-modelo-fama-french-3-fatores/)

[Introdução as Opções](http://clubedefinancas.com.br/materias/introducao-ao-mercado-de-opcoes/)

[Processos Estocásticos](http://clubedefinancas.com.br/materias/processos-estocasticos-para-financas-uma-introducao/)

[Simulação Monte Carlo](http://clubedefinancas.com.br/materias/simulacao-de-monte-carlo/)

[Modelo B&S](http://clubedefinancas.com.br/wp-content/uploads/2018/10/BS.html)

[Smile de Volatilidade](http://clubedefinancas.com.br/materias/smile-de-volatilidade/)

[Smile de Volatilidade parte 2](http://clubedefinancas.com.br/materias/smile-de-volatilidade-parte-2/)

[Superfícies de Volatilidade](http://clubedefinancas.com.br/materias/superficies-de-volatilidade/)

[Calibrando um SVI](http://clubedefinancas.com.br/materias/calibrando-um-svi/)

[Superfície SVI](http://clubedefinancas.com.br/materias/superficie-svi/)





# Imagens que ocupam 100% da largura do texto

<figure>
  <p><img class=scaled src="st-tropez.jpg"
    alt="St. Tropez">
  <figcaption>Saint Tropez and its
    fort in the evening sun</figcaption>
</figure>

And this is the style sheet:

figure {
  float: right;
  width: 30%;
  text-align: center;
  font-style: italic;
  font-size: smaller;
  text-indent: 0;
  border: thin silver solid;
  margin: 0.5em;
  padding: 0.5em;
}
img.scaled {
  width: 100%;
}
